package handler

import (
	"net/http"

	"awesomeProject/domain/server"
	"awesomeProject/tool"
)

//请求参数结构体
type UpgradeHeroPara struct {
	//用户Id  => 用户账号
	UserId string `valid:"required"`
	//出战英雄编号
	HeroNo int `valid:"required"`
}

//响应结构体
type UpgradeHeroResp struct {
	//状态码
	Code int `json:"Code"`
	//错误信息
	Info string `json:"Info"`
	//返回数据
	Data interface{} `json:"Data"`
}

//用户升级英雄
func UpgradeHero(w http.ResponseWriter, r *http.Request) {

	var (
		//声明请求结构体
		reqPara UpgradeHeroPara
		//接口请求方式
		requestMethod = "PUT"
	)

	//请求方式验证、数据格式验证、反序列化
	ErrResp, connect := Check(r, requestMethod, &reqPara)
	if ErrResp != nil {

		errResp := UpgradeHeroResp{
			Code: ErrResp.Code,
			Info: ErrResp.Info,
			Data: struct{}{},
		}
		err := tool.WriteResponse(w, errResp)
		throwPanic(err, "请求响应出现数据化序列化或响应数据写入出现错误")
		return
	}

	//初始化用户Id  => 用户账号（数据库）
	userAccount := reqPara.UserId
	//用户想要升级的英雄编号
	heroNo := reqPara.HeroNo

	//升级英雄业务逻辑
	commonResp, transaction := server.UpgradeHero(connect, userAccount, heroNo)
	if commonResp.Code != 0 { //事务回滚
		transaction.Rollback()
	}

	Resp := UpgradeHeroResp{
		Code: commonResp.Code,
		Info: commonResp.Info,
		Data: commonResp.Data,
	}
	err := tool.WriteResponse(w, Resp)
	throwPanic(err, "请求响应出现数据化序列化或响应数据写入出现错误")
	return

}
