package server

import (
	"math"
	"sync"

	"awesomeProject/domain/obj"
	"awesomeProject/tool"

	"github.com/jinzhu/gorm"
)

type MatchFightResponse struct {
	TargetUserId  string      `json:"TargetUserId"`
	TargetSolider int         `json:"TargetSolider"`
	IsSuccess     int         `json:"IsSuccess"`
	Resource      []Resources `json:"Resource"`
}

//玩家匹配战斗
func MatchFight(connect *gorm.DB, userAccount string, HeroNo int) (*CommonResp, *gorm.DB) {

	var (
		lock          sync.Mutex         //锁
		user          obj.TUser          //用户结构体
		heroInfo      obj.TUserHero      //用户英雄结构体
		userResources obj.TUserResources //用户资源结构体
		matchHall     obj.TMatchHall     //匹配大厅
	)
	//关闭数据库
	defer CloseDataFailure(connect)

	/**

	以下双重循环为随机抽取匹配大厅玩家

	原理:  利用map在遍历时，key的顺序被随机化。

	*/
	var defenders string //匹配到的防守方在map中的键值key，key值为UUID
Exit:
	for true {
		//从map中(防守方登记)读取 , map 在range时， 是随机的。
		for k, v := range MatchHallList {
			//当匹配到自己时，重新匹配
			if v.UserAccount == userAccount {
				break
			}
			lock.Lock() //匹配用户时  加锁
			if v.IsGame == false {
				defenders = k //匹配到的防守玩家在map中的key值，key值为UUID
				MatchHallList[k].IsGame = true
				lock.Unlock() //匹配成功解锁
				break Exit
			}
		}
	}
	// 逻辑执行结束后，修改玩家匹配的状态为 => 等待中
	defer func() {
		MatchHallList[defenders].IsGame = false
	}()

	// 开启事务
	transaction := connect.Begin()

	//查询攻击方的UUID,和用户积分
	userInfo, err := user.GetUUId(transaction, userAccount)
	if err != nil {
		return &CommonResp{
			Code: 4,
			Info: "查询不到攻击方的UUID",
			Data: struct{}{}}, transaction
	}

	//查询攻击方出战的英雄信息
	Info, err := heroInfo.GetHeroInfo(transaction, userInfo.Id, HeroNo)
	if err != nil { //查询英雄信息出现错误时
		return &CommonResp{
			Code: 5,
			Info: "查询英雄信息出现错误",
			Data: struct{}{}}, transaction
	}
	//获取攻击方当前士兵数量
	soldier, err := userResources.GetOneResources(transaction, userInfo.Id, tool.SOLDIER)
	if err != nil { //查询英雄信息出现错误时
		return &CommonResp{
			Code: 6,
			Info: "查询用户士兵资源出现错误",
			Data: struct{}{}}, transaction
	}
	if soldier == 0 { //攻击方士兵为0时
		return &CommonResp{
			Code: 601,
			Info: "您的士兵资源总数为0，不可进行匹配",
			Data: struct{}{}}, transaction
	}

	defenseUuid := defenders                           //防守方玩家UUID
	defenseSoldier := MatchHallList[defenders].Soldier //防守方玩家士兵资源总数
	defenseFight := MatchHallList[defenders].HeroFight //防守方玩家英雄战斗力

	//得到 用户英雄战斗力  攻击方总战力  防守方方总战力
	fight, userFight, defenseFightTotal := heroDamage(Info, soldier, defenseFight, defenseSoldier)

	if userFight >= defenseFightTotal { //攻击方获胜 ， 相等时 算攻击方 总战力高

		//消耗兵力
		//胜利方消耗拥有总士兵数 * (5 / 英雄战斗力) + 5 * 失败方关数士兵数
		//失败方消耗拥有总士兵数 * (8 / 英雄战斗力) + 10 * 失败方关数士兵数
		//用户消耗士兵数 (向上取整)
		reduceUserSoldier := int(math.Ceil(float64(soldier)*(float64(5)/float64(fight)))) + 5*1
		//关卡消耗士兵数(向上取整)
		reduceDefenseSoldier := int(math.Ceil(float64(defenseSoldier)*(float64(8)/float64(defenseFight)))) + 10*1

		//对攻击方进行数据库操作
		Com, err := User(transaction, user, userInfo, userResources, defenseUuid, soldier-reduceUserSoldier, reduceDefenseSoldier, true)
		if err != nil { // 在处理 攻击方 数据库过程中 出现错误
			return Com, transaction
		}
		//对防守方进行数据库操纵
		ComTarget, err := Target(transaction, userResources, user, defenseUuid, reduceDefenseSoldier, false)
		if err != nil { // 在处理 防守方 数据库过程中 出现错误
			return ComTarget, transaction
		}

		//判断防守方剩余士兵资源数量是否小于0或大于0，并执行对应操作
		ComJudge, err := Judge(transaction, matchHall, defenseUuid, reduceDefenseSoldier)
		if err != nil { // 在处理 防守方 剩余士兵资源是否为0 出现错误
			return ComJudge, transaction
		}
		//事务提交
		transaction.Commit()
		return Com, transaction
	}

	if userFight < defenseFightTotal { //防守方胜
		//消耗兵力
		//胜利方消耗拥有总士兵数 * (5 / 英雄战斗力) + 5 * 失败方关数士兵数
		//失败方消耗拥有总士兵数 * (8 / 英雄战斗力) + 10 * 失败方关数士兵数
		//用户消耗士兵数 (向上取整)
		reduceUserSoldier := int(math.Ceil(float64(soldier)*(float64(8)/float64(fight)))) + 10*1
		//关卡消耗士兵数(向上取整)
		reduceDefenseSoldier := int(math.Ceil(float64(defenseSoldier)*(float64(5)/float64(defenseFight)))) + 5*1

		//攻击方对数据库操作
		Com, err := User(transaction, user, userInfo, userResources, defenseUuid, soldier-reduceUserSoldier, reduceDefenseSoldier, false)
		if err != nil { // 在处理 攻击方 数据库过程中 出现错误
			return Com, transaction
		}
		//对防守方进行数据库操纵
		ComTarget, err := Target(transaction, userResources, user, defenseUuid, reduceDefenseSoldier, true)

		if ComTarget != nil {
			return ComTarget, transaction
		}

		//判断防守方剩余士兵资源数量是否小于0或大于0，并执行对应操作
		ComJudge, err := Judge(transaction, matchHall, defenseUuid, reduceDefenseSoldier)
		if ComTarget != nil { // 在处理 防守方 剩余士兵资源是否为0 出现错误
			return ComJudge, transaction
		}

		transaction.Commit()
		defer TransactionRollBack(transaction, err)
		// 操作过程中出现错误，修改玩家匹配的状态为 => 等待中
		defer func() {
			if err != nil {
				MatchHallList[defenders].IsGame = false
			}
		}()
		return &CommonResp{
			Code: 0,
			Info: "success",
			Data: Com}, transaction
	}

	return nil, transaction

}

/**
攻击方在战斗过程中的数据库操作

@param transaction       事务
@param user              用户结构体
@param userInfo          用户信息
@param userResources     用户资源结构体
@param defenseUuid       防守方UUID
@param soldier           攻击方最新士兵资源数
@param isWin             是否胜利

@return CommonResp       返回结构体

*/
func User(transaction *gorm.DB, user obj.TUser, userInfo *obj.TUser, userResources obj.TUserResources, defenseUuid string, soldier, reduceDefenseSoldier int, isWin bool) (*CommonResp, error) {

	soldierNow := soldier
	//当攻击方士兵小于0时
	if soldier < 0 {
		soldierNow = 0
	}

	if isWin == true { //攻击方获胜
		//攻击方积分+1
		err := user.UpdateIntegral(transaction, userInfo, userInfo.UserIntegral+1)
		if err != nil { //更新攻击方积分出现错误
			return &CommonResp{
				Code: 7,
				Info: "更新攻击方积分出现错误",
				Data: struct{}{}}, err
		}
	}

	//更新数据库攻击方士兵资源
	err := userResources.UpdateSoldier(transaction, userInfo.Id, soldierNow)
	if err != nil { //更新攻击方士兵资源出现错误
		return &CommonResp{
			Code: 8,
			Info: "更新攻击方士兵资源出现错误",
			Data: struct{}{}}, err
	}

	//获取攻击方资源
	resourcesList, err := userResources.GetResources(transaction, userInfo.Id)
	if err != nil { //将防守方从匹配大厅表软删除失败
		return &CommonResp{
			Code: 9,
			Info: "获取攻击方三项资源失败",
			Data: struct{}{}}, err
	}

	//转变资源数据结构
	resList := getNewResourcesList(resourcesList)

	//防守方剩余士兵计算
	var TargetSolider int
	if MatchHallList[defenseUuid].Soldier-reduceDefenseSoldier > 0 {
		TargetSolider = MatchHallList[defenseUuid].Soldier - reduceDefenseSoldier
	} else {
		TargetSolider = 0
	}

	//成功返回
	Suc := &MatchFightResponse{
		TargetUserId:  defenseUuid,
		TargetSolider: TargetSolider,
		IsSuccess:     1,
		Resource:      resList,
	}

	return &CommonResp{
		Code: 0,
		Info: "success",
		Data: Suc}, nil

}

/**
防守方在战斗过程中的数据库操作

@param transaction       事务
@param userResources     用户资源结构体
@param user              用户结构体
@param defenseUuid       防守方UUID
@param isWin             是否胜利


@return CommonResp       返回结构体

*/
func Target(transaction *gorm.DB, userResources obj.TUserResources, user obj.TUser, defenseUuid string, reduceDefenseSoldier int, isWin bool) (*CommonResp, error) {

	var soldierNow int

	if isWin == true { // 防守方胜利

		//查询防守方用户信息
		defenseInfo, err := user.GetInfo(transaction, defenseUuid)
		if err != nil { //查询防守方用户信息出现错误
			return &CommonResp{
				Code: 10,
				Info: "查询防守方用户信息出现错误",
				Data: struct{}{}}, err
		}
		//防守方积分+1
		err = user.UpdateIntegral(transaction, defenseInfo, defenseInfo.UserIntegral+1)
		if err != nil { //更新防守方积分出现错误
			return &CommonResp{
				Code: 11,
				Info: "更新防守方积分出现错误",
				Data: struct{}{}}, err
		}

	}

	//查询防守方士兵总资源数
	defenseSoldierTotal, err := userResources.GetOneResources(transaction, defenseUuid, tool.SOLDIER)
	if err != nil { //查询防守方士兵资源出现错误
		return &CommonResp{
			Code: 12,
			Info: "查询防守方士兵资源出现错误",
			Data: struct{}{}}, err
	}

	//计算战斗消耗后防守方总士兵资源数
	if defenseSoldierTotal-reduceDefenseSoldier > 0 {
		soldierNow = defenseSoldierTotal - reduceDefenseSoldier
	} else {
		soldierNow = 0
	}

	//更新数据库对应得防守方士兵资源
	err = userResources.UpdateSoldier(transaction, defenseUuid, soldierNow)
	if err != nil { //更新防守方士兵资源出现错误
		return &CommonResp{
			Code: 13,
			Info: "更新防守方士兵资源出现错误",
			Data: struct{}{}}, err
	}

	return nil, nil

}

/**
防守方在战斗消耗的士兵是否超过登记时的士兵数

@param transaction                  事务
@param matchHall                    匹配大厅结构体
@param defenseUuid                  防守方UUID
@param reduceDefenseSoldier         防守方在战斗过程中消耗的士兵数


@return CommonResp       返回结构体

*/
func Judge(transaction *gorm.DB, matchHall obj.TMatchHall, defenseUuid string, reduceDefenseSoldier int) (*CommonResp, error) {

	//计算防守方士兵是否为0
	if MatchHallList[defenseUuid].Soldier-reduceDefenseSoldier <= 0 { //当防守方士兵资源小于等于0时

		//退出大厅，删除数据库中得记录
		err := matchHall.DeleteRecord(transaction, defenseUuid)
		if err != nil { //将防守方从匹配大厅表软删除失败
			return &CommonResp{
				Code: 14,
				Info: "将防守方从匹配大厅表软删除失败",
				Data: struct{}{}}, err
		}
		delete(MatchHallList, defenseUuid) //防守方从游戏大厅(map)中删除

	} else { //防守方士兵资源大于0时

		//更新匹配大厅表得士兵资源数
		err := matchHall.UpdateSoldier(transaction, defenseUuid, MatchHallList[defenseUuid].Soldier-reduceDefenseSoldier)
		if err != nil { //将防守方从匹配大厅表软删除失败
			return &CommonResp{
				Code: 15,
				Info: "更新防守方在在匹配大厅表的士兵资源数，出现错误",
				Data: struct{}{}}, err
		}

		//更新map中防守方士兵资源数
		MatchHallList[defenseUuid].Soldier = MatchHallList[defenseUuid].Soldier - reduceDefenseSoldier
	}
	return nil, nil
}
