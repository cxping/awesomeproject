package models

/**

  用户表

*/

type TUser struct {
	Id           string //UUId
	UserAccount  string //用户账号， 全服唯一
	Name         string //用户昵称，全服唯一
	Salt         string //密码盐值
	Password     string //密码密文
	UserIntegral int    //用户积分
}
