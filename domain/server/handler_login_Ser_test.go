package server

import (
	"awesomeProject/tool"

	"github.com/stretchr/testify/assert"
	"testing"
)

func TestLogin(t *testing.T) {

	//初始化
	ast := assert.New(t)

	//填写 正确的 用户账号 ， 正确的 用户密码
	connect, _ := tool.Connection()
	t.Run("正确登录", func(t *testing.T) {
		//测试账号为错误账号，密码为错误/正确密码
		userId := "13275002095"
		password := "123456"
		Data := Login(connect, userId, password)
		t.Log("用户账号为：13275002095 ， 用户密码为：123456")
		ast.Equal("success", Data.Info)
	})

	//填写 错误的 用户账号 ， 正确的/错误的 用户密码
	connect, _ = tool.Connection()
	t.Run("错误登录", func(t *testing.T) {
		//测试账号为错误账号，密码为错误/正确密码
		userId := "myAccount"
		password := "A123456"
		Data := Login(connect, userId, password)
		t.Log("用户账号为：myAccount ， 用户密码为：A123456")
		ast.Equal("数据库中不存在该用户", Data.Info)
	})

	//填写 正确的 用户账号 ， 错误的 用户密码
	connect, _ = tool.Connection()
	t.Run("错误登录", func(t *testing.T) {
		//测试账号为错误账号，密码为错误/正确密码
		userId := "13275002095"
		password := "A123456"
		Data := Login(connect, userId, password)
		t.Log("用户账号为：13275002095 ， 用户密码为：A123456")
		ast.Equal("密码错误", Data.Info)
	})

}
