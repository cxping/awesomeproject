package handler

import (
	"fmt"
	"net/http"

	"awesomeProject/tool"

	"github.com/asaskevich/govalidator"
	"github.com/jinzhu/gorm"
)

//  返回结构体
type ReturnParameters struct {
	Code int         `json:"Code"`
	Info string      `json:"Info"`
	Data interface{} `json:"Data"`
}

//对请求方式、反序列化、数据格式验证进行封装统一验证
func Check(r *http.Request, requestMethod string, reqParams interface{}) (errResp *ReturnParameters, connect *gorm.DB) {

	//接口请求方式判断
	if r.Method != requestMethod {
		errResp := ReturnParameters{
			Code: 1,
			Info: "请求方式错误",
			Data: struct{}{},
		}
		return &errResp, nil
	}

	//请求方式正确时
	// 解析json
	err := tool.ParseRequest(r, reqParams)
	if err != nil {
		errResp := ReturnParameters{
			Code: 02,
			Info: "数据反序列化时出现错误",
			Data: struct{}{},
		}
		return &errResp, nil
	}

	//字段规则验证
	_, err = govalidator.ValidateStruct(reqParams)
	if err != nil { //当数据不符合规则时
		//返回错误码
		errResp := ReturnParameters{
			Code: 2,
			Info: "数据格式出现错误",
			Data: struct{}{},
		}
		return &errResp, nil
	}

	//连接数据库
	connect, err = tool.Connection()
	//连接数据库出现错误
	if err != nil {
		//返回错误码
		errResp := ReturnParameters{
			Code: 3,
			Info: "数据库连接错误",
			Data: struct{}{},
		}
		return &errResp, nil
	}

	return nil, connect
}

/**

  错误  -> 转异常
*/
func throwPanic(err error, info interface{}) {

	defer func() {
		if err != nil {
			fmt.Println(recover())
		}
	}()

	if err != nil { //判断是否错误,抛出异常
		panic(info)
	}
	return
}
