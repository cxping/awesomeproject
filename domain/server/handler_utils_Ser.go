package server

import (
	"awesomeProject/tool"
)

//响应结构体
type CommonResp struct {
	//状态码
	Code int
	//错误信息
	Info string
	//返回数据
	Data interface{}
}

//用户资源数据结构
type Resources struct {
	Type  tool.ResourceType `json:"Type"`
	Count int               `json:"count"`
}

//资源数据结构
type ResourcesList struct {
	//用户资源列表
	Resource []Resources `json:"Resource"`
}
