package tool

import (
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

func Connection() (*gorm.DB, error) {

	//db,err := gorm.Open("mysql","root:root@tcp(127.0.0.1:3306)/test?charset=utf8&parseTime=True&loc=Local")
	db, err := gorm.Open("mysql", "root:123456@tcp(193.112.99.54:3306)/test?charset=utf8&parseTime=True&loc=Local")
	if err != nil {
		return nil, err
	}
	//避免gorm在转义struct的名字时后缀加复数形式s
	db.SingularTable(true)

	return db, nil
}
