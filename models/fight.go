package models

/**

  系统初始化关卡配置表

*/

type Fight struct {
	No        int    //关卡编号
	HeroFight int    //关卡英雄战斗力
	Soldier   string //关卡英雄士兵数
	HeroNo    int    //通过关卡后解锁的英雄 编号
}
