package server

import (
	"awesomeProject/tool"

	"github.com/stretchr/testify/assert"
	"testing"
)

func TestGetResources(t *testing.T) {
	//初始化
	ast := assert.New(t)

	//填写 正确的 用户账号 ， 食物资源类型 1
	connect, _ := tool.Connection()
	t.Run("收取食物资源", func(t *testing.T) {

		userId := "13275002095"
		Type := 1
		Data := GetResources(connect, userId, Type)

		t.Log("用户账号为:1327500295 , 收取资源类型: 1 (食物)")
		ast.Equal("success", Data.Info)

	})

	//填写 正确的 用户账号 ， 士兵资源类型 2

	t.Run("收取士兵资源", func(t *testing.T) {
		connect, _ = tool.Connection()

		userId := "13275002095"
		Type := 2
		Data := GetResources(connect, userId, Type)

		t.Log("用户账号为:1327500295 , 收取资源类型: 2 (士兵)")
		ast.Equal("success", Data.Info)

	})

	//填写 正确的 用户账号 ， 金币资源类型 3

	t.Run("收取金币资源", func(t *testing.T) {
		connect, _ = tool.Connection()

		userId := "13275002095"
		Type := 3
		Data := GetResources(connect, userId, Type)

		t.Log("用户账号为:1327500295 , 收取资源类型: 3 (金币)")
		ast.Equal("success", Data.Info)

	})

	//填写 正确的 用户账号 ， 食物资源类型异常(超过 资源类型范围 1 2 3 之外的数字)

	t.Run("输入异常资源类型", func(t *testing.T) {
		connect, _ = tool.Connection()

		userId := "13275002095"
		Type := 7
		Data := GetResources(connect, userId, Type)

		t.Log("用户账号为:1327500295 , 收取资源类型: 1 (食物)")
		ast.Equal("资源类型异常", Data.Info)

	})

	//填写 错误的 用户账号 ， 正确的资源类型（1、2、3）

	t.Run("错误账号收取资源", func(t *testing.T) {
		connect, _ = tool.Connection()

		userId := "912446605"
		Type := 3
		Data := GetResources(connect, userId, Type)

		t.Log("用户账号为:912446605 , 收取资源类型: 3 (食物)")
		ast.Equal("获取用户英雄列表失败", Data.Info)

	})

	//填写 正确的 用户账号 ， 正确的资源类型（1、2、3）,
	//资源在规定时间内不可重复收取

	t.Run("不可在规定时间内重复收取同类型资源", func(t *testing.T) {
		connect, _ = tool.Connection()

		userId := "912446605@qq.com"
		Type := 3
		Data := GetResources(connect, userId, Type)

		t.Log("第一次收取资源:用户账号为:912446605@qq.com , 收取资源类型: 3(金兵)")
		ast.Equal("success", Data.Info)

		connect, _ = tool.Connection()
		Data = GetResources(connect, userId, Type)
		t.Log("第二次收取资源:用户账号为:912446605@qq.com , 收取资源类型: 3(金兵)")
		ast.Equal("收取资源时间未满1分钟", Data.Info)
	})

}
