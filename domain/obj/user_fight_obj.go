package obj

import (
	"awesomeProject/tool"

	"github.com/jinzhu/gorm"
)

type TUserFight struct {
	Id           string //Id
	UserId       string //用户表的UUID
	FightSoldier int    //关卡士兵数
	FightNo      int    //关卡编号
}

//设置与数据库对应的表名
func (TUserFight) TableName() string {
	return "t_user_fight"
}

/**

用户解锁关卡

@param  transaction   事务
@param  userId        用户UUId，主键

*/
func (userFight *TUserFight) UnLockFight(transaction *gorm.DB, userUId string) error {

	//初始化用户关卡  第一关
	fight := TUserFight{
		Id:           tool.CreateUUID(),
		UserId:       userUId,
		FightNo:      tool.INIT_FIGHT_NO,
		FightSoldier: tool.INIT_FIGHT_SOLDIER,
	}

	//创建关卡
	err := transaction.Create(&fight).Error

	return err
}

/**
获取用户关卡信息

@param  connect       数据库连接类
@param  userId        用户UUId，主键

@return *TUserFight   用户关卡结构体
@return error         错误

*/

func (userFight *TUserFight) GetSectionByUserId(connect *gorm.DB, userUId string) (*TUserFight, error) {

	err := connect.Select("fight_no,fight_soldier").Where("user_id = ?", userUId).Find(&userFight).Error

	return userFight, err
}

/**

更新用户关卡士兵总数

@param  connect           数据库连接类
@param  userId            用户UUID
@param  soldier           士兵总数


@return error             错误

*/

func (userFight *TUserFight) UpSectionSoldier(connect *gorm.DB, userId string, soldier int) error {

	err := connect.Table("t_user_fight").Where("user_id = ?", userId).Update("fight_soldier", soldier).Error

	return err
}

/**

用户通关后，解锁新的关卡

@param  connect           数据库连接类
@param  userId            用户UUID
@param  section           关卡编号


@return error             错误


*/
func (userFight *TUserFight) UpUnlockSection(connect *gorm.DB, userId string, section int) error {

	var fight Fight
	//查找新关卡信息
	fightInfo, err := fight.GetFightInfo(connect, section)
	if err != nil {
		return err
	}

	//更新用户关卡信息
	err = connect.Table("t_user_fight").Where("user_id = ?", userId).
		Updates(map[string]interface{}{"fight_no": fightInfo.No, "fight_soldier": fightInfo.Soldier}).Error

	return err

}

/**

用户通关全部通过后，删除用户关卡表信息

@param  connect           数据库连接类
@param  userId            用户UUID


@return error             错误

*/
func (userFight *TUserFight) DeleteSection(connect *gorm.DB, userId string) error {

	err := connect.Where("user_id = ?", userId).Delete(&userFight).Error

	return err

}
