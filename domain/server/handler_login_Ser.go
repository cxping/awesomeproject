package server

import (
	"awesomeProject/domain/obj"
	"awesomeProject/tool"

	"github.com/jinzhu/gorm"
)

//用户英雄数据结构
type Hero struct {
	No int `json:"No"`
	Lv int `json:"Lv"`
}

//资源数据结构
type HeroResourcesList struct {
	//用户资源列表
	Resource []Resources `json:"Resource"`
	//用户英雄列表
	Hero []Hero `json:"Hero"`
}

func Login(connect *gorm.DB, userAccount string, requestPassword string) *CommonResp {

	var (
		user          obj.TUser          //声明用户结构体
		userHero      obj.TUserHero      //数据库映射的用户英雄结构体
		userResources obj.TUserResources //数据库映射的用户资源结构体
	)
	defer CloseDataFailure(connect)

	//查找数据库是否存在该用户
	userInfo, userIsExist := user.IsExistByAccountReturnUser(connect, userAccount)
	if userIsExist == true { //当数据库查找到该数据时 存在就为false  不存在就为 true（它在下面一层就进行了对RecordNotFound错误的判定（当错误为RecordNotFound就返回true），所以是true）
		return &CommonResp{
			Code: 4,
			Info: "数据库中不存在该用户",
			Data: struct{}{}}
	}

	//密码盐值
	salt := userInfo.Salt
	//数据库密码密文
	password := userInfo.Password

	//密码验证
	isSuccess := tool.PasswordDecryption(password, salt, requestPassword)
	//密码错误
	if isSuccess == false {
		return &CommonResp{
			Code: 5,
			Info: "密码错误",
			Data: struct{}{}}
	}

	//获取用户资源集合
	userResourcesList, err := userResources.GetResources(connect, userInfo.Id)
	if err != nil { //获取玩家资源失败
		return &CommonResp{
			Code: 6,
			Info: "获取玩家资源失败",
			Data: struct{}{}}
	}

	//资源集合 重新 组合
	resList := getNewResourcesList(userResourcesList)

	//获取用户所拥有的英雄列表
	userHeroList := userHero.GetAllHero(connect, userInfo.Id)
	//英雄集合格式转化
	heroList := getNewHeroList(userHeroList)

	return &CommonResp{
		Code: 0,
		Info: "success",
		Data: HeroResourcesList{Resource: resList, Hero: heroList}}

}
