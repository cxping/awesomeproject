package models

/**

  用户英雄表（用户所拥有的英雄  1 -> 多 ）

*/
type TUserHero struct {
	Id        string //Id
	UserId    string //用户表的UUID
	HeroNo    int    //初始化英雄表的UUID
	Level     int    //英雄等级
	Wisdom    int    //英雄智慧
	Loyalty   int    //英雄忠诚
	Brave     int    //英雄英勇
	Diligence int    //英雄勤勉
}
