package obj

import (
	"github.com/jinzhu/gorm"
)

type Fight struct {
	No        int    //关卡编号
	HeroFight int    //关卡英雄战斗力
	Soldier   string //关卡英雄士兵数
	HeroNo    int    //通过关卡后解锁的英雄 编号
}

// 设置Hero的表名为`hero`
func (Fight) TableName() string {
	return "fight"
}

/**
通过关卡编号获取管卡信息

@param connect 数据库连接类
@param no      关卡编号

@return *Fight  关卡结构体
@return error  错误

*/
func (fight *Fight) GetFightInfo(connect *gorm.DB, no int) (*Fight, error) {

	err := connect.Where("no = ?", no).First(&fight).Error

	if err != nil {
		return nil, err
	}

	return fight, nil

}
