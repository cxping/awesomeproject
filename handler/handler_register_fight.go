package handler

import (
	"net/http"

	"awesomeProject/domain/server"
	"awesomeProject/tool"
)

//请求参数结构体
type RegisterFightPara struct {
	//用户Id  => 用户账号
	UserId string `valid:"required"`
	//出战英雄编号
	HeroNo int `valid:"required"`
	//出战士兵数
	Soldier int `valid:"required"`
}

//响应结构体
type RegisterFightResp struct {
	//状态码
	Code int `json:"Code"`
	//请求是否成功
	Info string `json:"Info"`
	//数据
	Data interface{} `json:"Data"`
}

func RegisterFight(w http.ResponseWriter, r *http.Request) {

	var (
		//声明请求结构体
		registerFightReq RegisterFightPara
		//请求方法
		reqMethod = "PUT"
	)

	//请求方式验证、数据格式验证、反序列化
	ErrResp, connect := Check(r, reqMethod, &registerFightReq)
	if ErrResp != nil {

		errResp := SectionFightResp{
			Code: ErrResp.Code,
			Info: ErrResp.Info,
			Data: struct{}{},
		}
		err := tool.WriteResponse(w, errResp)
		throwPanic(err, "请求响应出现数据化序列化或响应数据写入出现错误")
		return
	}

	userAccount := registerFightReq.UserId //用户user_account
	fightHero := registerFightReq.HeroNo   //出战英雄编号
	Soldier := registerFightReq.Soldier    //出战士兵资源数

	//用户登记大厅逻辑ser
	commonResp, transaction, UserId := server.RegisterFight(connect, userAccount, fightHero, Soldier)
	if commonResp.Code != 0 { //事务回滚
		transaction.Rollback()
	}
	Resp := UpgradeHeroResp{
		Code: commonResp.Code,
		Info: commonResp.Info,
		Data: commonResp.Data,
	}
	err := tool.WriteResponse(w, Resp)
	if err != nil && commonResp.Code == 0 { //出错删除内存中的防守玩家
		delete(server.MatchHallList, UserId)
	}
	throwPanic(err, "请求响应出现数据化序列化或响应数据写入出现错误")
	return

}
