package server

import (
	"awesomeProject/tool"

	"github.com/stretchr/testify/assert"
	"testing"
)

func TestSectionFight(t *testing.T) {

	//初始化
	ast := assert.New(t)

	//正确的用户账号，正确的英雄编号
	//通关成功
	connect, _ := tool.Connection()
	t.Run("正确用户账号，正确英雄编号", func(t *testing.T) {

		userAccount := "13275002095"
		heroNo := 1
		Data, transaction := SectionFight(connect, userAccount, heroNo)
		if Data.Code != 0 { //事务回滚
			transaction.Rollback()
		}

		t.Log("用户账号为: 13275002095 ， 英雄编号为: 1")
		ast.Equal("success", Data.Info)
	})

	//正确的用户账号，正确的英雄编号
	//通关失败，士兵先消耗完
	t.Run("正确用户账号，正确英雄编号", func(t *testing.T) {
		connect, _ = tool.Connection()
		userAccount := "D123456"
		heroNo := 1
		Data, transaction := SectionFight(connect, userAccount, heroNo)
		if Data.Code != 0 { //事务回滚
			transaction.Rollback()
		}

		t.Log("用户账号为: D123456 ， 英雄编号为: 1")
		ast.Equal("success", Data.Info)
	})

	//正确的用户账号，正确的英雄编号
	//通关成功，用户和关卡士兵一起消耗完
	t.Run("正确用户账号，正确英雄编号", func(t *testing.T) {
		connect, _ = tool.Connection()
		userAccount := "F123456"
		heroNo := 1
		Data, transaction := SectionFight(connect, userAccount, heroNo)
		if Data.Code != 0 { //事务回滚
			transaction.Rollback()
		}

		t.Log("用户账号为: D123456 ， 英雄编号为: 1")
		ast.Equal("success", Data.Info)
	})

	//错误的用户账号，正确/错误的英雄编号

	t.Run("错误用户账号,正确/错误英雄编号", func(t *testing.T) {
		connect, _ = tool.Connection()
		userAccount := "912446605"
		heroNo := 1
		Data, transaction := SectionFight(connect, userAccount, heroNo)
		if Data.Code != 0 { //事务回滚
			transaction.Rollback()
		}

		t.Log("用户账号为: 13275002095 ， 英雄编号为: 1")
		ast.Equal("数据库查询不到用户UUID", Data.Info)
	})

	//正确的用户账号，正确/错误的英雄编号
	//由某种原因造成用户数据库关卡表无对应关卡
	t.Run("用户关卡表无对应关卡", func(t *testing.T) {
		connect, _ = tool.Connection()
		userAccount := "912446605@qq.com"
		heroNo := 1
		Data, transaction := SectionFight(connect, userAccount, heroNo)
		if Data.Code != 0 { //事务回滚
			transaction.Rollback()
		}

		t.Log("用户账号为: 13275002095 ， 英雄编号为: 1")
		ast.Equal("用户关卡查询失败", Data.Info)
	})

	//正确的用户账号，正确/错误的英雄编号
	//由某种原因造成用户数据库关卡表存在超出关卡配置表的关卡
	t.Run("用户关卡表对应关卡超出关卡配置表", func(t *testing.T) {
		connect, _ = tool.Connection()
		userAccount := "A123456"
		heroNo := 1
		Data, transaction := SectionFight(connect, userAccount, heroNo)
		if Data.Code != 0 { //事务回滚
			transaction.Rollback()
		}

		t.Log("用户账号为: 13275002095 ， 英雄编号为: 1")
		ast.Equal("查询关卡信息出现错误", Data.Info)
	})

	//正确的用户账号，错误的英雄编号

	t.Run("正确用户账号，错误英雄编号", func(t *testing.T) {
		connect, _ = tool.Connection()
		userAccount := "B123456"
		heroNo := 2
		Data, transaction := SectionFight(connect, userAccount, heroNo)
		if Data.Code != 0 { //事务回滚
			transaction.Rollback()
		}

		t.Log("用户账号为: B123456 ， 英雄编号为: 2")
		ast.Equal("查询英雄信息出现错误", Data.Info)
	})

	//正确的用户账号，正确的英雄编号
	//由某种原因造成用户数据库资源表不存在士兵资源
	t.Run("用户关卡表对应关卡超出关卡配置表", func(t *testing.T) {
		connect, _ = tool.Connection()
		userAccount := "C123456"
		heroNo := 1
		Data, transaction := SectionFight(connect, userAccount, heroNo)
		if Data.Code != 0 { //事务回滚
			transaction.Rollback()
		}

		t.Log("用户账号为: 13275002095 ， 英雄编号为: 1")
		ast.Equal("查询用户士兵资源出现错误", Data.Info)
	})

	//正确的用户账号，正确的英雄编号
	//玩家通过全部关卡
	t.Run("用戶通过所有关卡	", func(t *testing.T) {
		connect, _ = tool.Connection()
		userAccount := "E123456"
		heroNo := 1
		Data, transaction := SectionFight(connect, userAccount, heroNo)
		if Data.Code != 0 { //事务回滚
			transaction.Rollback()
		}

		t.Log("用户账号为: E123456 ， 英雄编号为: 1")
		ast.Equal("您已通关", Data.Info)
	})

	//正确的用户账号，正确的英雄编号
	//通关后关卡配置表，下一关关卡不存在
	t.Run("用戶过关后，下一关关卡不存在", func(t *testing.T) {
		connect, _ = tool.Connection()
		userAccount := "13275002095"
		heroNo := 1
		Data, transaction := SectionFight(connect, userAccount, heroNo)
		if Data.Code != 0 { //事务回滚
			transaction.Rollback()
		}

		t.Log("用户账号为: 13275002095 ， 英雄编号为: 1")
		ast.Equal("通过关卡后，解锁关卡失败", Data.Info)
	})

}
