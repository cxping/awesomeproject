package obj

import (
	"time"

	"awesomeProject/tool"
	"github.com/jinzhu/gorm"
)

type TMatchHall struct {
	Id          string    //UUId
	UserId      string    //用户UUID
	PlayHeroNo  int       //出战英雄编号
	PlaySoldier int       //出战士兵资源数
	HeroFight   int       //出战英雄战斗力
	DeleteAt    time.Time //是否删除

	TUser TUser `gorm:"ForeignKey:UserId;AssociationForeignKey:Id"`
}

type JoinUser struct {
	UserId      string //用户UUID
	UserAccount string //用户账号
	PlayHeroNo  int    //出战英雄编号
	PlaySoldier int    //出战士兵资源数
	HeroFight   int    //出战英雄战斗力

}

/**

创建防守方游戏大厅记录

@param connect     数据库连接类
@param userId      用户UUID
@param fightHero   出战英雄编号
@param fight       出战英雄战斗力
@param Soldier     出战士兵数

@return error    错误

*/
func (hall *TMatchHall) CreateRecord(connect *gorm.DB, userId string, fightHero, fight, Soldier int) error {

	var record = TMatchHall{
		Id:          tool.CreateUUID(),
		UserId:      userId,
		PlayHeroNo:  fightHero,
		PlaySoldier: Soldier,
		HeroFight:   fight}

	err := connect.Create(record).Error

	return err
}

/**

查找防守方游戏大厅记录

@param connect   数据库连接类
@param userId    用户UUID

@return error    错误

*/
func (hall *TMatchHall) IsExistRecord(connect *gorm.DB, userId string) bool {

	IsExist := connect.Where("user_id = ?", userId).Find(&hall).RecordNotFound()

	return IsExist
}

/**

删除防守方游戏大厅记录

@param connect   数据库连接类
@param userId    用户UUID

@return error    错误

*/
func (hall *TMatchHall) DeleteRecord(connect *gorm.DB, userId string) error {

	err := connect.Where("user_id = ?", userId).Delete(&hall).Error

	return err
}

/**

更新防守方游戏大厅记录的士兵数记录

@param connect   数据库连接类
@param userId    用户UUID

@return error    错误

*/
func (hall *TMatchHall) UpdateSoldier(connect *gorm.DB, userId string, soldier int) error {

	err := connect.Table("t_match_hall").Where("user_id = ?", userId).Updates(map[string]interface{}{"play_soldier": soldier}).Error

	return err
}

/**

@param connect   数据库连接类

@return []*JoinUser    匹配大厅用户数据结构

*/
func (hall *TMatchHall) JoinUser(connect *gorm.DB) []*JoinUser {

	var list []*JoinUser

	connect.Select(
		"t_match_hall.user_id," +
			"t_match_hall.hero_fight," +
			"t_match_hall.play_hero_no," +
			"t_match_hall.play_soldier," +
			"t_user.user_account").Table("t_match_hall").Joins("left join t_user on" +
		" t_match_hall.user_id = t_user.id").Scan(&list)

	return list
}
