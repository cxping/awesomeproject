package obj

import (
	"time"

	"awesomeProject/tool"

	"github.com/jinzhu/gorm"
)

type TUserResources struct {
	Id               string            //UUId
	UserId           string            //用户表的UUID
	Type             tool.ResourceType //用户拥有的资源类型  资源类型: 1=>食物， 2=>士兵， 3=>金币
	ResourcesTotal   int               //用户拥有的资源总数
	LastResourceTime time.Time         //用户最后一次收取资源时间
}

//设置与数据库对应的表名
func (TUserResources) TableName() string {
	return "t_user_resources"
}

/**

初始化用户食物（初始食物值为1000）

@param  transaction   事务
@param  userId        用户UUId，主键

*/

func (userResources *TUserResources) InitUserResources(transaction *gorm.DB, userUId string) (err error) {

	//生成Id
	uuidFood := tool.CreateUUID()
	uuidSoldier := tool.CreateUUID()
	uuidGold := tool.CreateUUID()

	//初始化用户食物
	userInitFood := TUserResources{
		Id:               uuidFood,
		UserId:           userUId,
		Type:             tool.FOOD,
		ResourcesTotal:   tool.INIT_USER_FOOD,
		LastResourceTime: time.Now(), // 函数返回格式数据:2017-04-11 12:52:52.794351777 +0800 CST
	}
	//初始化用户士兵
	userInitSoldier := TUserResources{
		Id:               uuidSoldier,
		UserId:           userUId,
		Type:             tool.SOLDIER,
		ResourcesTotal:   tool.INIT_USER_SOLDIER,
		LastResourceTime: time.Now(), // 函数返回格式数据:2017-04-11 12:52:52.794351777 +0800 CST
	}
	//初始化用户金币
	userInitGold := TUserResources{
		Id:               uuidGold,
		UserId:           userUId,
		Type:             tool.GOLD,
		ResourcesTotal:   tool.INIT_USER_GOID,
		LastResourceTime: time.Now(), // 函数返回格式数据:2017-04-11 12:52:52.794351777 +0800 CST
	}

	//创建
	err = transaction.Create(&userInitFood).Error
	if err != nil {

		return err
	}
	err = transaction.Create(&userInitSoldier).Error
	if err != nil {

		return err
	}
	err = transaction.Create(&userInitGold).Error
	if err != nil {

		return err
	}

	return nil
}

/**
获取用户的所有资源

@param  transaction       事务
@param  userId            用户UUId，主键

@return []TUserResources  用户资源数据对象集合
*/

func (userResources *TUserResources) GetResources(connect *gorm.DB, userUId string) ([]TUserResources, error) {

	var userResourcesList []TUserResources

	err := connect.Select("id,type,resources_total").Where("user_id = ?", userUId).Find(&userResourcesList).Error
	if err != nil {
		return nil, err
	}

	return userResourcesList, nil

}

/**
获取用户的单项资源总数

@param  transaction       事务
@param  userId            用户UUId，主键
@param  Type              资源类型

@return ResourcesTotal    用户资源总数
@return error             错误

*/

func (userResources *TUserResources) GetOneResources(connect *gorm.DB, userUId string, Type tool.ResourceType) (int, error) {

	err := connect.Select("resources_total").Where("user_id = ? AND type = ?", userUId, Type).Find(&userResources).Error

	return userResources.ResourcesTotal, err

}

/**

升级英雄更新相应金币和食物资源

@param  transaction       事务
@param  idAndTypeMap      id 和 资源类型 map
@param  userFoodNow       用户升级后的食物
@param  userGoldNow       用户升级后的金币
@param  userUId           用户UUId，主键

@return userFoodResources 用户资源结构体
@return userGoldResources 用户资源结构体

*/
func (userResources *TUserResources) UpgradeUpdateResources(transaction *gorm.DB, idAndTypeMap map[tool.ResourceType]string, userFoodNow int, userGoldNow int, userUId string) (
	userFoodResources, userGoldResources *TUserResources, err error) {

	userFoodResources = &TUserResources{Id: idAndTypeMap[tool.FOOD]}
	userGoldResources = &TUserResources{Id: idAndTypeMap[tool.GOLD]}

	//更新用户食物
	err = transaction.Model(&userFoodResources).Update("resources_total", userFoodNow).Error
	if err != nil {
		return nil, nil, err
	}
	//更新用户金币
	err = transaction.Model(&userGoldResources).Update("resources_total", userGoldNow).Error
	if err != nil {
		return nil, nil, err
	}

	return userFoodResources, userGoldResources, nil
}

/**

用户收取资源更新相关资源总数

@param  connect           数据库连接类
@param  Type              资源类型
@param  resourcesTotal    收取资源后的资源总数
@param  userUId           用户UUId，主键

@return error

*/
func (userResources *TUserResources) IncreaseResources(connect *gorm.DB, Type int, resourcesTotal int, userId string) error {

	err := connect.Table("t_user_resources").Where("user_id =? AND type =?", userId, Type).
		Updates(map[interface{}]interface{}{"resources_total": resourcesTotal, "last_resource_time": time.Now()}).Error

	if err != nil {
		return err
	}
	return nil

}

/**

获取用户单项资源的最后一次收取时间 和 资源总数

@param  connect           数据库连接类
@param  Type              资源类型
@param  userId            用户UUID

@return *TUserResources   用户资源结构体
@return error             错误

*/
func (userResources *TUserResources) GetLastTime(connect *gorm.DB, Type int, userId string) (*TUserResources, error) {

	err := connect.Select("last_resource_time,resources_total").Where("user_id = ? And type = ?", userId, Type).Find(&userResources).Error

	if err != nil {
		return nil, err
	}
	return userResources, nil

}

/**

更新用户士兵总数

@param  connect           数据库连接类
@param  userId            用户UUID
@param  soldier            士兵总数


@return error             错误

*/

func (userResources *TUserResources) UpdateSoldier(connect *gorm.DB, userId string, soldier int) error {

	err := connect.Table("t_user_resources").Where("user_id = ? AND type = ?", userId, tool.SOLDIER).Update("resources_total", soldier).Error

	return err
}
