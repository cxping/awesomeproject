package tool

import (
	"crypto/md5"
	"encoding/hex"
	"math/rand"
	"time"
)

/**
         用户密码验证

  password          数据库密码密文
  salt              用户盐值
  validatePassword  请求密码明文

*/
func PasswordDecryption(password string, salt string, validatePassword string) bool {

	pass := MD5(validatePassword, salt)

	//将请求的密码根据盐值加密后  与   数据库用户的密码密文进行比较
	if pass != password {
		return false
	}

	return true
}

// 生成32位MD5
func MD5(password string, salt string) string {

	md := md5.New()

	md.Write([]byte(password + salt))

	return hex.EncodeToString(md.Sum(nil))
}

//生成随机字符串  参数为长度
func GetRandomString(length int) string {
	str := "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
	bytes := []byte(str)
	result := []byte{}
	r := rand.New(rand.NewSource(time.Now().UnixNano()))
	for i := 0; i < length; i++ {
		result = append(result, bytes[r.Intn(len(bytes))])
	}
	return string(result)
}
