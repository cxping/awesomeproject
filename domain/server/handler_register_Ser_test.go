package server

import (
	"awesomeProject/tool"

	"github.com/stretchr/testify/assert"
	"testing"
)

func TestRegisterLogic(t *testing.T) {

	//初始化
	ast := assert.New(t)

	//填写 不存在的 用户账号 ， 不存在的 用户昵称
	connect, _ := tool.Connection()
	t.Run("账号注册成功", func(t *testing.T) {
		userId := "912446605"
		name := "蔡小平X号"
		password := "123456"
		Data, transaction := RegisterLogic(connect, userId, name, password)
		if Data.Code != 0 { //事务回滚
			transaction.Rollback()
		}

		t.Log("用户账号为:912446605 ，用户昵称:蔡小平2号 , 用户密码为：123456")
		ast.Equal("Success", Data.Info)

	})

	//填写 存在的 用户账号 ， 存在/不存在的 用户昵称
	connect, _ = tool.Connection()
	t.Run("账号已被注册", func(t *testing.T) {
		userId := "13275002095"
		name := "蔡小平"
		password := "123456"
		Data, transaction := RegisterLogic(connect, userId, name, password)
		if Data.Code != 0 { //事务回滚
			transaction.Rollback()
		}

		t.Log("用户账号为：13275002095 ，用户昵称:蔡小平 ，用户密码为：123456")
		ast.Equal("该用户账号已存在", Data.Info)
	})

	//填写 不存在的用户账号 ， 存在的 用户昵称
	connect, _ = tool.Connection()
	t.Run("昵称已被注册", func(t *testing.T) {
		userId := "912446605"
		name := "蔡小平"
		password := "123456"
		Data, transaction := RegisterLogic(connect, userId, name, password)
		if Data.Code != 0 { //事务回滚
			transaction.Rollback()
		}

		t.Log("用户账号为：912446605 ，用户昵称:蔡小平 ，用户密码为：123456")
		ast.Equal("该用户昵称已存在", Data.Info)
	})

}
