package handler

import (
	"net/http"

	"awesomeProject/domain/server"
	"awesomeProject/tool"
)

//请求参数结构体
type MatchFightPara struct {
	//用户Id  => 用户账号
	UserId string `valid:"required"`
	//出战英雄编号
	HeroNo int `valid:"required"`
}

//响应结构体
type MatchFightResp struct {
	//状态码
	Code int `json:"Code"`
	//请求是否成功
	Info string `json:"Info"`
	//数据
	Data interface{} `json:"Data"`
}

func MatchFight(w http.ResponseWriter, r *http.Request) {

	var (
		//声明请求结构体
		matchFightPara MatchFightPara
		//请求方法
		reqMethod = "PUT"
	)

	//请求方式验证、数据格式验证、反序列化
	ErrResp, connect := Check(r, reqMethod, &matchFightPara)
	if ErrResp != nil {

		errResp := SectionFightResp{
			Code: ErrResp.Code,
			Info: "failure",
			Data: struct{}{},
		}
		err := tool.WriteResponse(w, errResp)
		throwPanic(err, "请求响应出现数据化序列化或响应数据写入出现错误")
		return
	}

	userAccount := matchFightPara.UserId //用户user_account
	HeroNo := matchFightPara.HeroNo      //出战英雄编号

	commonResp, transaction := server.MatchFight(connect, userAccount, HeroNo)
	if commonResp.Code != 0 { //事务回滚
		transaction.Rollback()
	}

	Resp := SectionFightResp{
		Code: commonResp.Code,
		Info: commonResp.Info,
		Data: commonResp.Data,
	}
	err := tool.WriteResponse(w, Resp)
	throwPanic(err, "请求响应出现数据化序列化或响应数据写入出现错误")
	return

}
