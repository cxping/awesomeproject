package handler

import (
	"net/http"

	"awesomeProject/domain/obj"
	"awesomeProject/domain/server"
	"awesomeProject/tool"
)

type Handler func(w http.ResponseWriter, r *http.Request)

var AllHandlers = map[string]Handler{}

//注册路由
func registerHandler(path string, h Handler) {
	AllHandlers[path] = h
}

func HandlerBase(next Handler) Handler {
	return func(w http.ResponseWriter, r *http.Request) {

		//当服务器启动时，加载防守玩家信息
		InitPlay()
		next(w, r)
	}
}

//防守方玩家加载
func InitPlay() {
	var user obj.TMatchHall

	connect, _ := tool.Connection()

	info := user.JoinUser(connect)

	for _, v := range info {
		server.MatchHallList[v.UserId] = &server.MatchHall{
			UserAccount: v.UserAccount,
			HeroNo:      v.PlayHeroNo,
			HeroFight:   v.HeroFight,
			Soldier:     v.PlaySoldier,
			IsGame:      false}
	}
}
