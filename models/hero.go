package models

/**

  系统初始化英雄表

*/

type Hero struct {
	No            int    //英雄编号（前端自己输 ， 后端不进行生成）
	Checkpoint    int    //英雄关卡
	Name          string //英雄名字
	InitLevel     int    //英雄等级
	InitWisdom    int    //初始化智慧
	InitLoyalty   int    //初始化忠诚
	InitBrave     int    //初始化英勇
	InitDiligence int    //初始化勤勉
}
