package tool

import (
	"github.com/satori/go.uuid"
)

func CreateUUID() string {

	UUID, _ := uuid.NewV4()
	uuidToString := UUID.String()

	return uuidToString

}
