package main

import (
	"awesomeProject/routers"
	"fmt"
	"log"
	"net/http"

	_ "github.com/jinzhu/gorm/dialects/mysql"
)

//var db *gorm.DB

const (

	//监听端口 ，  修改请这里操作
	PORT = 12345
	//服务器地址， 修改请这里操作
	HOST = "localhost"
)

func addr() string {
	//端口和服务器地址  字符串拼接
	return fmt.Sprintf("%s:%d", HOST, PORT)
}

type User struct {
	ID   int
	Name string
}

func main() {

	//入口,路由列表
	routers.RegisterRouters()
	fmt.Println("服务器监听前面")
	err := http.ListenAndServe(addr(), nil)
	fmt.Println("服务器监听后面")
	if err != nil {
		log.Fatalf("Can't start the server: %s", err)
	}
}
