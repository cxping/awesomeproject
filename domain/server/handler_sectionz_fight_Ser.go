package server

import (
	"math"

	"awesomeProject/domain/obj"
	"awesomeProject/tool"

	"github.com/jinzhu/gorm"
)

type sectionFightResp struct {
	IsSuccess int         `json:"IsSuccess"`
	Resource  []Resources `json:"Resource"`
}

//开始关卡战斗
func SectionFight(connect *gorm.DB, userAccount string, heroNo int) (*CommonResp, *gorm.DB) {

	var (
		userFight     obj.TUserFight     //用户关卡结构体
		user          obj.TUser          //用户结构体
		userHero      obj.TUserHero      //用户英雄结构体
		userResources obj.TUserResources //用户资源结构体
		Fight         obj.Fight          //关卡结构体
	)
	defer CloseDataFailure(connect)
	//事务开启
	transaction := connect.Begin()
	//通过userAccount 查找 userId
	userInfo, err := user.GetUUId(transaction, userAccount)
	if err != nil {
		return &CommonResp{ //查询用户UUID出现错误时
			Code: 4,
			Info: "数据库查询不到用户UUID",
			Data: struct{}{}}, transaction
	}

	//查找用户目前解锁的关卡信息（关卡编号， 和 关卡 士兵数）
	section, err := userFight.GetSectionByUserId(transaction, userInfo.Id)
	if err != nil {
		return &CommonResp{ //查询用户关卡查询错误时
			Code: 5,
			Info: "用户关卡查询失败",
			Data: struct{}{}}, transaction
	}
	//查找关卡对应信息
	fightInfo, err := Fight.GetFightInfo(transaction, section.FightNo)
	if err != nil { //查询英雄信息出现错误时
		return &CommonResp{
			Code: 6,
			Info: "查询关卡信息出现错误",
			Data: struct{}{}}, transaction
	}

	//查询用户出战的英雄信息
	heroInfo, err := userHero.GetHeroInfo(transaction, userInfo.Id, heroNo)
	if err != nil { //查询英雄信息出现错误时
		return &CommonResp{
			Code: 7,
			Info: "查询英雄信息出现错误",
			Data: struct{}{}}, transaction
	}

	//获取用户当前士兵数量
	soldier, err := userResources.GetOneResources(transaction, userInfo.Id, tool.SOLDIER)
	if err != nil { //查询英雄信息出现错误时
		return &CommonResp{
			Code: 8,
			Info: "查询用户士兵资源出现错误",
			Data: struct{}{}}, transaction
	}

	//得到 用户英雄战斗力  用户总战力  关卡总战力
	fight, totalFight, sectionFight := heroDamage(heroInfo, soldier, fightInfo.HeroFight, section.FightSoldier)

	//	判断是否解锁成功
	userSoldierNow, sectionSoldierNow, isSuccess := userFightLogic(totalFight, sectionFight, soldier, section.FightSoldier, fight, section.FightNo, fightInfo.HeroFight)
	if isSuccess == false {
		//解锁失败
		CommRes, _ := unlockFailure(userResources, userFight, transaction, userInfo.Id, sectionSoldierNow, userSoldierNow)

		return &CommonResp{
			Code: CommRes.Code,
			Info: CommRes.Info,
			Data: CommRes.Data}, transaction

	}

	//解锁成功
	CommRes, _ := unlockSuccess(userResources, userHero, userFight, transaction, userInfo.Id, fightInfo.HeroNo, section.FightNo, userSoldierNow)

	return &CommonResp{
		Code: CommRes.Code,
		Info: CommRes.Info,
		Data: CommRes.Data}, transaction

}

/**

用户解锁下一关成功

@param userResources    用户资源结构体
@param userHero         用户英雄结构体
@param userFight        用户关卡结构体
@param transaction      事务
@param userId           用户UUID
@param heroNO           用户获得的新英雄编号
@param fightNo          用户当前关卡编号
@param userSoldierNow   用户解锁成功后，剩余士兵资源数量


@return  CommonResp     返回结构体

*/
func unlockSuccess(userResources obj.TUserResources, userHero obj.TUserHero, userFight obj.TUserFight,
	transaction *gorm.DB, userId string, heroNO, fightNo, userSoldierNow int) (*CommonResp, error) {

	//更新用户士兵数
	err := userResources.UpdateSoldier(transaction, userId, userSoldierNow)
	if err != nil {
		return &CommonResp{
			Code: 9,
			Info: "通过关卡后，更新用户士兵资源总数失败",
			Data: struct{}{}}, err
	}
	//判断下一关是否为最后一关
	if fightNo+1 > tool.LAST_SECTION {

		//删除用户关卡表的记录
		err = userFight.DeleteSection(transaction, userId)
		if err != nil {
			return &CommonResp{
				Code: 14,
				Info: "全部通关后，删除用户关卡表信息失败",
				Data: struct{}{}}, err
		}
		transaction.Commit()
		return &CommonResp{
			Code: 10,
			Info: "您已通关",
			Data: struct{}{}}, err
	}

	//解锁下一关 关卡
	err = userFight.UpUnlockSection(transaction, userId, fightNo+1)
	if err != nil {
		return &CommonResp{
			Code: 11,
			Info: "通过关卡后，解锁关卡失败",
			Data: struct{}{}}, err
	}
	//获得新英雄
	err = userHero.GetNewHero(transaction, heroNO, userId)
	if err != nil {
		return &CommonResp{
			Code: 12,
			Info: "通过关卡后，为用户新增英雄失败",
			Data: struct{}{}}, err
	}

	//查找玩家当前的资源
	resourcesList, err := userResources.GetResources(transaction, userId)
	if err != nil {
		return &CommonResp{
			Code: 13,
			Info: "解锁关卡成功，获取用户士兵、金币、食物资源出现错误",
			Data: struct{}{}}, err
	}

	//用户资源集合
	resList := getNewResourcesList(resourcesList)

	transaction.Commit()
	return &CommonResp{
		Code: 0,
		Info: "success",
		Data: sectionFightResp{
			IsSuccess: 1,
			Resource:  resList}}, nil

}

/**

用户解锁下一关失败

@param userResources     用户资源结构体
@param userFight         用户关卡结构体
@param transaction       事务
@param userId            用户UUID
@param sectionSoldierNow 解锁失败后，用户当前关卡剩余士兵资源数量
@param userSoldierNow   用户解锁失败后，用户剩余士兵资源数量

@return  CommonResp     返回结构体

*/
func unlockFailure(userResources obj.TUserResources, userFight obj.TUserFight, transaction *gorm.DB,
	userId string, sectionSoldierNow, userSoldierNow int) (*CommonResp, error) {

	//解锁关卡失败
	//更新数据库中 用户关卡表 中的的士兵资源数
	err := userFight.UpSectionSoldier(transaction, userId, sectionSoldierNow)
	if err != nil {
		return &CommonResp{
			Code: 14,
			Info: "用户士兵提前消耗完,在更新用户关卡表士兵资源数时，出现更新错误",
			Data: struct{}{}}, err
	}
	//更新用户士兵资源数
	err = userResources.UpdateSoldier(transaction, userId, userSoldierNow)
	if err != nil {
		return &CommonResp{
			Code: 15,
			Info: "未通过关卡后，更新用户士兵资源总数失败",
			Data: struct{}{}}, err
	}

	//查找玩家当前的资源
	resourcesList, err := userResources.GetResources(transaction, userId)
	if err != nil {
		return &CommonResp{
			Code: 16,
			Info: "解锁关卡失败，获取用户士兵、金币、食物资源出现错误",
			Data: struct{}{}}, err
	}

	//循环用户资源集合
	resList := getNewResourcesList(resourcesList)

	transaction.Commit()
	return &CommonResp{
		Code: 0,
		Info: "success",
		Data: sectionFightResp{
			IsSuccess: 0,
			Resource:  resList}}, nil

}

/**
用户战斗逻辑

@param  soldier                  用户士兵总数
@param  sectionFightSoldier      关卡士兵总数
@param  fight                    用户英雄战斗力
@param  sectionFightNo           关卡编号（关卡关数）
@param  sectionHeroFight         关卡英雄战斗力

@return  soldier                 用户士兵数
@return  sectionFightSoldier     关卡士兵数
*/

func userFightLogic(totalFight, sectionFight, soldier, sectionFightSoldier, fight, sectionFightNo, sectionHeroFight int) (userSoldierNow, sectionSoldierNow int, isSuccess bool) {

	//胜利方消耗拥有总士兵数 * (5 / 英雄战斗力) + 5 * 关卡关数
	//失败方消耗拥有总士兵数 * (8 / 英雄战斗力) + 10 * 关卡关数

	var reduceUserSoldier, reduceSectionSoldier int

	for true {

		if totalFight >= sectionFight { //用户总战力高

			//用户消耗士兵数 (向上取整)
			reduceUserSoldier = int(math.Ceil(float64(soldier)*(float64(5)/float64(fight)))) + 5*sectionFightNo
			//关卡消耗士兵数(向上取整)
			reduceSectionSoldier = int(math.Ceil(float64(sectionFightSoldier)*(float64(8)/float64(sectionHeroFight)))) + 10*sectionFightNo
		}
		if totalFight < sectionFight { //关卡总战力高

			//用户消耗士兵数(向上取整)
			reduceUserSoldier = int(math.Ceil(float64(soldier)*(float64(8)/float64(fight)))) + 10*sectionFightNo
			//关卡消耗士兵数(向上取整)
			reduceSectionSoldier = int(math.Ceil(float64(sectionFightSoldier)*(float64(5)/float64(sectionHeroFight)))) + 5*sectionFightNo

		}

		//关卡士兵 和 用户士兵 资源都未消耗完
		if sectionFightSoldier-reduceSectionSoldier > 0 && soldier-reduceUserSoldier > 0 {

			//赋值用户最新的士兵数
			soldier = soldier - reduceUserSoldier
			//赋值关卡最新的士兵数
			sectionFightSoldier = sectionFightSoldier - reduceSectionSoldier

		}

		//关卡士兵先消耗完毕，解锁成功
		if sectionFightSoldier-reduceSectionSoldier <= 0 && soldier-reduceUserSoldier > 0 {
			isSuccess = true
			sectionSoldierNow = 0
			return soldier, sectionFightSoldier, true
		}
		//用户士兵先消耗完毕，解锁失败
		if sectionFightSoldier-reduceSectionSoldier > 0 && soldier-reduceUserSoldier <= 0 {
			soldier = 0
			isSuccess = false
			return soldier, sectionFightSoldier, false
		}
		//用户士兵  和  关卡士兵 一起消耗完 ， 解锁成功
		if sectionFightSoldier-reduceSectionSoldier <= 0 && soldier-reduceUserSoldier <= 0 {
			soldier = 0
			sectionSoldierNow = 0
			isSuccess = true
			return soldier, sectionSoldierNow, true
		}
	}
	return
}
