package obj

import (
	"awesomeProject/tool"

	"github.com/jinzhu/gorm"
)

type TUser struct {
	Id           string //Id
	UserAccount  string //用户Id， 全服唯一
	Name         string //用户昵称，全服唯一
	Salt         string //密码盐值
	Password     string //密码密文
	UserIntegral int    //用户积分
}

// 设置TUser的表名为`t_user`
func (TUser) TableName() string {
	return "t_user"
}

/**

根据userAccount(全服唯一)查询是否存在用户

@param    transaction   事务
@param    userAccount   用户账号(全服唯一)

@return    count   用户记录数
@return    error   错误
*/
func (user *TUser) IsExistByAccount(transaction *gorm.DB, userAccount string) (count int, err error) {

	var countByUserId int

	err = transaction.Model(&TUser{}).Where("user_account = ?", userAccount).Count(&countByUserId).Error

	return countByUserId, err
}

/**

根据userAccount(全服唯一)查询是否存在用户

@param    connect  数据库连接类
@param    userId   用户账号(全服唯一)

@return    user   用户
@return    userIsExist   是否存在该用户
*/
func (user *TUser) IsExistByAccountReturnUser(connect *gorm.DB, userAccount string) (*TUser, bool) {

	userIsExist := connect.Select("id,password,salt").Where("user_account = ?", userAccount).Find(&user).RecordNotFound()

	return user, userIsExist
}

/**

根据name查询是否存在用户，并返回记录数

@param    transaction  事务
@param    name         用户昵称(全服唯一)

@return    countByName       用户记录数
*/
func (user *TUser) IsExistByName(transaction *gorm.DB, name string) (count int, err error) {

	var countByName int

	err = transaction.Model(&TUser{}).Where("name = ?", name).Count(&countByName).Error

	return countByName, err

}

/**

创建用户

@param  transaction   事务
@param  userAccount   用户账号，全服唯一
@param  name          用户昵称，全服唯一
@param  password      用户密码

@return createUser.Id 用户UUID
*/
func (user *TUser) CreateUser(transaction *gorm.DB, userAccount string, name string, password string) (uid string, err error) {

	//角色密码盐值生成
	salt := tool.GetRandomString(6)
	//给角色密码加盐加密
	saltPassword := tool.MD5(password, salt)
	//生成UUID
	uuid := tool.CreateUUID()

	//初始化用户
	createUser := TUser{
		Id:           uuid,
		UserAccount:  userAccount,
		Name:         name,
		Salt:         salt,
		Password:     saltPassword,
		UserIntegral: tool.INTEGRAL, //用户积分初始化
	}
	//数据库新增用户
	err = transaction.Create(&createUser).Error
	if err != nil {

		return "", err
	}

	return createUser.Id, nil
}

/**

通过 用户账号（userAccount） 来获取用户Id

@param  connect       数据库连接类
@param  userAccount   用户账号，全服唯一

@return   *TUser            TUser类
*/
func (user *TUser) GetUUId(connect *gorm.DB, userAccount string) (*TUser, error) {

	//通过 用户账号（userAccount） 查询用户
	err := connect.Select("id,user_account,user_integral").Where("user_account = ?", userAccount).Find(&user).Error

	return user, err
}

/**

通过 用户账号（userAccount） 来获取用户信息

@param  connect       数据库连接类
@param  userId        用户UUID

@return   *TUser            TUser类
*/
func (user *TUser) GetInfo(connect *gorm.DB, userId string) (*TUser, error) {

	var userInfo *TUser
	userInfo = &TUser{}

	err := connect.Where("id = ?", userId).First(&userInfo).Error

	return userInfo, err
}

/**

增加用户积分

@param connect   数据库连接类
@param userInfo  用户尸体类
@param integral  用户积分

@return error    错误

*/

func (user *TUser) UpdateIntegral(connect *gorm.DB, userInfo *TUser, integral int) error {

	err := connect.Model(&userInfo).Update("user_integral", integral).Error

	return err
}
