package handler

import (
	"net/http"

	"awesomeProject/domain/server"
	"awesomeProject/tool"
)

// 请求参数  结构体
type loginPara struct {
	UserId   string `valid:"required"`
	Password string `valid:"required"`
}

//响应结构体
type LoginResp struct {
	//状态码
	Code int `json:"Code"`
	//错误信息
	Info string `json:"Info"`
	//返回数据
	Data interface{} `json:"Data"`
}

//用户登录接口
func Login(w http.ResponseWriter, r *http.Request) {

	var (
		//声明请求结构体
		reqPara loginPara
		//接口请求方式
		reqMethod = "POST"
	)

	//请求方式验证、数据格式验证、反序列化
	ErrResp, connect := Check(r, reqMethod, &reqPara)
	if ErrResp != nil {
		Resp := LoginResp{
			Code: ErrResp.Code,
			Info: ErrResp.Info,
			Data: ErrResp.Data,
		}
		err := tool.WriteResponse(w, Resp)
		throwPanic(err, "请求响应出现数据化序列化或响应数据写入出现错误")
		return
	}

	//用户Id,全服唯一（请求参数）
	userAccount := reqPara.UserId
	//用户密码(请求参数)
	requestPassword := reqPara.Password
	//用户登陆逻辑
	commonResp := server.Login(connect, userAccount, requestPassword)

	Resp := LoginResp{
		Code: commonResp.Code,
		Info: commonResp.Info,
		Data: commonResp.Data,
	}
	err := tool.WriteResponse(w, Resp)
	throwPanic(err, "请求响应出现数据化序列化或响应数据写入出现错误")

	return
}
