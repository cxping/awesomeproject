package server

import (
	"awesomeProject/domain/obj"
	"awesomeProject/tool"

	"github.com/jinzhu/gorm"
)

var MatchHallList = make(map[string]*MatchHall) //游戏大厅玩家

type MatchHall struct {
	UserAccount string //用户账号
	HeroNo      int    //出战英雄
	HeroFight   int    //出战英雄攻击力
	Soldier     int    //出战的士兵数
	IsGame      bool   // 是否处于游戏状态
}

func RegisterFight(connect *gorm.DB, userAccount string, fightHero, Soldier int) (*CommonResp, *gorm.DB, string) {

	var (
		user          obj.TUser          //用户结构体
		userHero      obj.TUserHero      //用户英雄结构体
		userResources obj.TUserResources //用户资源结构体
		matchHall     obj.TMatchHall     //用户匹配大厅结构体
	)
	defer CloseDataFailure(connect)
	//开启事务
	transaction := connect.Begin()

	//查询用户UUid
	userId, err := user.GetUUId(transaction, userAccount)
	if err != nil {
		return &CommonResp{ //查询用户UUID出现错误时
			Code: 4,
			Info: "数据库查询不到用户UUID",
			Data: struct{}{}}, transaction, ""
	}

	//查找是否重复登记
	isFind := matchHall.IsExistRecord(transaction, userId.Id)
	if isFind == false {
		return &CommonResp{ //数据库查询不到用户出战的英雄
			Code: 5,
			Info: "不能重复登记",
			Data: struct{}{}}, transaction, ""
	}

	//查找用户是否拥有该出战英雄
	heroInfo, err := userHero.CheckHero(transaction, fightHero, userId.Id)
	if err != nil {
		return &CommonResp{ //数据库查询不到用户出战的英雄
			Code: 6,
			Info: "数据库查询不到用户出战的英雄",
			Data: struct{}{}}, transaction, ""
	}

	//查询出战士兵数有没超过自身资源数
	soldierTotal, err := userResources.GetOneResources(transaction, userId.Id, tool.SOLDIER)
	if err != nil {
		return &CommonResp{ //数据库查询士兵资源出现错误
			Code: 7,
			Info: "数据库查询士兵资源出现错误",
			Data: struct{}{}}, transaction, ""
	}

	if soldierTotal-Soldier < 0 {
		return &CommonResp{ //出战士兵总数大于数据库士兵资源总数
			Code: 8,
			Info: "出战士兵总数大于数据库士兵资源总数",
			Data: struct{}{}}, transaction, ""
	}

	//计算出战英雄战斗力
	fight := heroInfo.Diligence + heroInfo.Brave + heroInfo.Loyalty + heroInfo.Wisdom

	//进行登记（写入数据库）
	err = matchHall.CreateRecord(transaction, userId.Id, fightHero, fight, Soldier)
	if err != nil {
		return &CommonResp{ //将用户登记到匹配大厅表失败时
			Code: 9,
			Info: "将用户登记到匹配大厅表失败",
			Data: struct{}{}}, transaction, ""
	}

	//查询玩家资源
	resourcesList, err := userResources.GetResources(transaction, userId.Id)
	if err != nil { //收取资源成功后，获取玩家资源失败
		return &CommonResp{
			Code: 10,
			Info: "收取资源成功后，获取玩家资源失败",
			Data: struct{}{}}, transaction, ""
	}
	resList := getNewResourcesList(resourcesList)

	transaction.Commit()

	//在内存中将登记的玩家记录下来
	MatchHallList[userId.Id] = &MatchHall{
		UserAccount: userId.UserAccount,
		HeroNo:      fightHero,
		HeroFight:   fight,
		Soldier:     Soldier,
		IsGame:      false}

	return &CommonResp{ //查询用户UUID出现错误时
		Code: 0,
		Info: "success",
		Data: ResourcesList{Resource: resList}}, transaction, userId.Id

}
