package tool

//属性类型
type AttrType int16

const (
	WISDOM    AttrType = 1 //智慧
	LOYALTY   AttrType = 2 //忠诚
	BRAVE     AttrType = 3 //英勇
	DILIGENCE AttrType = 4 //勤勉
)

//资源类型
type ResourceType int16

const (
	FOOD    ResourceType = 1 //食物
	SOLDIER ResourceType = 2 //士兵
	GOLD    ResourceType = 3 //金币
)

//初始化配置参数
const (

	/**
	用户初始化（注册）相关参数配置
	*/
	INTEGRAL              = 0    //用户积分
	INIT_USER_HERO_NUMBER = 1    //用户初始化英雄编号
	HERO_LEVEL            = 1    //初始化英雄等级
	INIT_USER_FOOD        = 1000 //用户初始化食物数量
	INIT_USER_GOID        = 1000 //用户初始化金币数量
	INIT_USER_SOLDIER     = 1000 //用户初始化士兵数量
	INIT_ATTRIBUTE        = 1    //用户属性初始值

	/**
	  用户初始化相关关卡配置
	*/

	INIT_FIGHT_NO      = 1  //初始化关卡编号
	INIT_FIGHT_SOLDIER = 80 //初始化关卡士兵

	/**
	用户升级英雄相关属性参数
	*/

	UPGRADE_LEVEL     = 1 //英雄等级
	UPGRADE_BRAVE     = 3 //英雄英勇值
	UPGRADE_DILIGENCE = 1 //英雄勤勉值
	UPGRADE_LOYALTY   = 1 //英雄忠诚值
	UPGRADE_WISDOM    = 1 //英雄智慧值

	/**
	关卡相关参数配置
	*/
	LAST_SECTION = 5 // 关卡最后一关

)
