package server

import (
	"awesomeProject/tool"

	"github.com/stretchr/testify/assert"
	"testing"
)

func TestUpgradeHero(t *testing.T) {

	//初始化
	ast := assert.New(t)

	//错误用户账号 ， 正确英雄编号 。 资源充足
	connect, _ := tool.Connection()
	t.Run("错误的用户账号", func(t *testing.T) {

		userId := "13275002095"
		heroNo := 1
		Data, transaction := UpgradeHero(connect, userId, heroNo)
		if Data.Code != 0 { //事务回滚
			transaction.Rollback()
		}

		t.Log("用户账号为: 912446605 ， 英雄编号为: 1")
		ast.Equal("success", Data.Info)
	})

	//填写 错误的用户账号  ， 正确/错误的英雄编号

	t.Run("错误的用户账号", func(t *testing.T) {
		connect, _ = tool.Connection()
		userId := "912446605"
		heroNo := 1
		Data, transaction := UpgradeHero(connect, userId, heroNo)
		if Data.Code != 0 { //事务回滚
			transaction.Rollback()
		}

		t.Log("用户账号为: 912446605 ， 英雄编号为: 1")
		ast.Equal("数据库中不存在该用户", Data.Info)
	})

	//填写 正确的用户账号  ， 错误的英雄编号

	t.Run("正确用户账号，错误英雄编号", func(t *testing.T) {
		connect, _ = tool.Connection()
		userId := "13275002095"
		heroNo := 6
		Data, transaction := UpgradeHero(connect, userId, heroNo)
		if Data.Code != 0 { //事务回滚
			transaction.Rollback()
		}

		t.Log("用户账号为: 13275002095 ， 英雄编号为: 6")
		ast.Equal("查询英雄信息出现错误", Data.Info)
	})

	//填写 正确的用户账号  ， 英雄等级为100的英雄编号

	t.Run("正确用户账号，等级100英雄编号", func(t *testing.T) {
		connect, _ = tool.Connection()
		userId := "13275002095"
		heroNo := 5
		Data, transaction := UpgradeHero(connect, userId, heroNo)
		if Data.Code != 0 { //事务回滚
			transaction.Rollback()
		}

		t.Log("用户账号为: 13275002095 ， 英雄编号为: 5")
		ast.Equal("英雄等级已到达满级100级", Data.Info)
	})

	//填写 正确的用户账号  ， 正确的英雄编号 ， 食物不足情况。

	t.Run("正确用户账号，英雄编号。食物不足", func(t *testing.T) {
		connect, _ = tool.Connection()
		userId := "B123456"
		heroNo := 1
		Data, transaction := UpgradeHero(connect, userId, heroNo)
		if Data.Code != 0 { //事务回滚
			transaction.Rollback()
		}

		t.Log("用户账号为: 13275002095 ， 英雄编号为: 1")
		ast.Equal("用户食物不足,请充值", Data.Info)
	})

	//填写 正确的用户账号  ， 正确的英雄编号 ， 金币不足情况。

	t.Run("正确用户账号，英雄编号。金币不足", func(t *testing.T) {
		connect, _ = tool.Connection()
		userId := "A123456"
		heroNo := 1
		Data, transaction := UpgradeHero(connect, userId, heroNo)
		if Data.Code != 0 { //事务回滚
			transaction.Rollback()
		}

		t.Log("用户账号为: 13275002095 ， 英雄编号为: 1")
		ast.Equal("用户食物不足,请充值", Data.Info)
	})

}
