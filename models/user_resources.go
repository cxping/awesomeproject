package models

import "time"

/**

  用户资源表（用户所拥有的资源）

   用户与资源  1 -> 多 关系

*/
type TUserResources struct {
	Id               string    //UUId
	UserId           string    //用户表的UUID
	Type             int       //用户拥有的资源类型  资源类型: 1=>食物， 2=>士兵， 3=>金币
	resourcesTotal   int       //用户拥有的资源总数
	LastResourceTime time.Time //用户最后一次收取资源时间
}
