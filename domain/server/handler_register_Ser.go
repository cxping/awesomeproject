package server

import (
	"awesomeProject/domain/obj"

	"github.com/jinzhu/gorm"
)

func RegisterLogic(connect *gorm.DB, userAccount string, name string, password string) (*CommonResp, *gorm.DB) {

	var (
		user          obj.TUser          //用户结构体
		userHero      obj.TUserHero      //用户英雄结构体
		userResources obj.TUserResources //用户资源结构体
		userFight     obj.TUserFight     //用户关卡结构体
	)
	defer CloseDataFailure(connect)

	//事务开启
	transaction := connect.Begin()

	//根据用户账号 userAccount 查找数据库是否存在该User
	countByUserAccount, err := user.IsExistByAccount(transaction, userAccount)
	//判断数据库操作是否执行错误
	if err != nil {
		return &CommonResp{
			Code: 4,
			Info: "查询用户失败",
			Data: struct{}{}}, transaction
	}

	//判断用户记录数
	if countByUserAccount != 0 {
		return &CommonResp{
			Code: 5,
			Info: "该用户账号已存在",
			Data: struct{}{}}, transaction
	}

	//查找数据库是否存在该用户昵称
	countByName, err := user.IsExistByName(transaction, name)
	//判断数据库操作是否执行错误
	if err != nil {
		return &CommonResp{
			Code: 6,
			Info: "查询用户昵称失败",
			Data: struct{}{}}, transaction
	}

	//判断用户记录数
	if countByName != 0 {
		return &CommonResp{
			Code: 7,
			Info: "该用户昵称已存在",
			Data: struct{}{}}, transaction
	}

	//创建用户信息
	userUId, err := user.CreateUser(transaction, userAccount, name, password)
	//判断数据库操作是否执行错误
	if err != nil {
		return &CommonResp{
			Code: 8,
			Info: "数据库创建用户失败",
			Data: struct{}{}}, transaction
	}

	//初始化英雄
	err = userHero.CreateInitUserHero(transaction, userUId)
	//判断数据库操作是否执行错误
	if err != nil {
		return &CommonResp{
			Code: 9,
			Info: "数据库初始化英雄失败",
			Data: struct{}{}}, transaction
	}

	//初始化用户资源
	err = userResources.InitUserResources(transaction, userUId)
	//判断初始化用户资源是否失败
	if err != nil {
		return &CommonResp{
			Code: 10,
			Info: "数据库初始化用户资源失败",
			Data: struct{}{}}, transaction
	}

	//初始化解锁关卡
	err = userFight.UnLockFight(transaction, userUId)
	if err != nil {
		return &CommonResp{
			Code: 11,
			Info: "用户初始化关卡失败",
			Data: struct{}{}}, transaction
	}

	//事务提交
	transaction.Commit()

	return &CommonResp{
		Code: 0,
		Info: "Success",
		Data: struct{}{}}, transaction
}
