package server

import (
	"awesomeProject/domain/obj"
	"awesomeProject/tool"

	"github.com/jinzhu/gorm"
)

type LevelAndResources struct {
	//英雄当前等级
	Lv int
	//玩家当前拥有资源
	Resource []UpgradeHeroResources
}

//用户资源结构体
type UpgradeHeroResources struct {
	Type  tool.ResourceType `json:"Type"`
	Count int               `json:"count"`
}

func UpgradeHero(connect *gorm.DB, userAccount string, heroNo int) (*CommonResp, *gorm.DB) {

	var (
		user          obj.TUser          //用户结构体
		userHero      obj.TUserHero      // 用户英雄结构体
		userResources obj.TUserResources // 用户资源结构体
	)
	defer CloseDataFailure(connect)
	//开启事务
	transaction := connect.Begin()

	//根据用户账号 查询用户
	userInfo, userHeroIsExist := user.IsExistByAccountReturnUser(transaction, userAccount)
	if userHeroIsExist == true { //用户不存在时
		return &CommonResp{
			Code: 4,
			Info: "数据库中不存在该用户",
			Data: struct{}{}}, transaction
	}

	//先获取到该英雄当前等级
	heroInfo, err := userHero.GetHeroInfo(transaction, userInfo.Id, heroNo)
	if err != nil { //查询英雄信息出现错误时
		return &CommonResp{
			Code: 5,
			Info: "查询英雄信息出现错误",
			Data: struct{}{}}, transaction
	}

	//判断英雄等级有没超过最高等级 100级
	if heroInfo.Level >= 100 { //超过100级时，不能在进行英雄升级操作
		return &CommonResp{
			Code: 6,
			Info: "英雄等级已到达满级100级",
			Data: struct{}{}}, transaction
	}

	//当前升级需要的金币总数和食物总数
	resourcesTotal := heroInfo.Level * 10
	//获取用户当前金币、食物、士兵资源总数
	userResourcesList, err := userResources.GetResources(transaction, userInfo.Id)
	if err != nil { //获取玩家资源失败
		return &CommonResp{
			Code: 7,
			Info: "获取玩家资源失败",
			Data: struct{}{}}, transaction
	}
	//资源和资源类型 map , id 和 资源类型 map
	resourcesTypeMap, idTypeMap := ChangeType(userResourcesList)
	//升级完成之后的资源数  = 根据现有食物 - 消耗升级所需要的食物资源数
	userFoodNow := resourcesTypeMap[tool.FOOD] - resourcesTotal
	//升级完成之后的资源数  = 根据现有金币 - 消耗升级所需要的金币资源数
	userGoldNow := resourcesTypeMap[tool.GOLD] - resourcesTotal

	//升一级消耗 10*当前等级的食物
	if userFoodNow < 0 { //食物不足时
		return &CommonResp{
			Code: 8,
			Info: "用户食物不足,请充值",
			Data: struct{}{}}, transaction
	}

	//升一级消耗 10*当前等级的金币
	if userGoldNow < 0 { //金币不足时
		return &CommonResp{
			Code: 9,
			Info: "用户金币不足,请充值",
			Data: struct{}{}}, transaction
	}

	//满足升级条件，更新用户金币和食物资源
	foodNow, goldNow, err := userResources.UpgradeUpdateResources(transaction, idTypeMap, userFoodNow, userGoldNow, userInfo.Id)
	if err != nil {
		return &CommonResp{
			Code: 10,
			Info: "数据库更新用户资源失败",
			Data: struct{}{}}, transaction
	}

	//金币和食物数量满足条件时，提升英雄的等级1级，智慧、忠诚、勤勉增加1点，英勇增加3点
	userHeroId, heroLevel, heroBrave, heroDiligence, heroLoyalty, heroWisdom := UpgradeAttribute(heroInfo)

	//升级用户英雄等级和属性值
	heroLevelNow, err := userHero.UpGradeHeroById(transaction, heroInfo, userHeroId, heroLevel, heroBrave, heroDiligence, heroLoyalty, heroWisdom)
	if err != nil {
		return &CommonResp{
			Code: 11,
			Info: "数据库更新用户等级和属性值失败",
			Data: struct{}{}}, transaction
	}

	// 升级后的用户资源集合
	resList := ResourcesStructure(foodNow.ResourcesTotal, goldNow.ResourcesTotal, resourcesTypeMap[tool.SOLDIER])

	//事务提交
	transaction.Commit()

	return &CommonResp{
		Code: 0,
		Info: "success",
		Data: LevelAndResources{Lv: heroLevelNow, Resource: resList}}, transaction
}

/**

类型转换 list -> map

@param userResourcesList 用户资源结构体数据集


@return resourcesTypeMap   资源和资源类型 map ,
@return idTypeMap          id 和 资源类型 map

*/

func ChangeType(userResourcesList []obj.TUserResources) (resourcesTypeMap map[tool.ResourceType]int, idTypeMap map[tool.ResourceType]string) {

	//资源和资源类型 map
	resourcesTypeMap = make(map[tool.ResourceType]int)
	//id 和 资源类型 map
	idTypeMap = make(map[tool.ResourceType]string)

	//将查询出来的资源 转换为  map 类型
	for _, resources := range userResourcesList {

		resourcesTypeMap[resources.Type] = resources.ResourcesTotal
		idTypeMap[resources.Type] = resources.Id

	}

	return resourcesTypeMap, idTypeMap

}

/**

升级用户英雄属性

@param      heroInfo       //用户英雄结构体

@return     userHeroId     //用户英雄表Id
@return 	heroLevel      //英雄等级
@return 	heroBrave      //英雄英勇值
@return 	heroDiligence  //英雄勤勉值
@return 	heroLoyalty    //英雄忠诚值
@return 	heroWisdom     //英雄智慧值


*/

func UpgradeAttribute(heroInfo *obj.TUserHero) (userHeroId string, heroLevel, heroBrave, heroDiligence, heroLoyalty, heroWisdom int) {

	//金币和食物数量满足条件时，提升英雄的等级1级，智慧、忠诚、勤勉增加1点，英勇增加3点
	userHeroId = heroInfo.Id                                    //用户英雄表Id
	heroLevel = heroInfo.Level + tool.UPGRADE_LEVEL             //英雄等级
	heroBrave = heroInfo.Brave + tool.UPGRADE_BRAVE             //英雄英勇值
	heroDiligence = heroInfo.Diligence + tool.UPGRADE_DILIGENCE //英雄勤勉值
	heroLoyalty = heroInfo.Loyalty + tool.UPGRADE_LOYALTY       //英雄忠诚值
	heroWisdom = heroInfo.Wisdom + tool.UPGRADE_WISDOM          //英雄智慧值

	return userHeroId, heroLevel, heroBrave, heroDiligence, heroLoyalty, heroWisdom

}

/**

玩家升级后的资源结构 整合 转化

@param  foodNow       //升级后的食物资源数
@param  goldNow       //升级后的金币资源数
@param  soldierNow    //升级后的士兵资源数

@return resList       //资源集合

*/
func ResourcesStructure(foodNow, goldNow, soldierNow int) (resList []UpgradeHeroResources) {

	resList = append(resList, UpgradeHeroResources{
		Type:  tool.FOOD,
		Count: foodNow,
	})
	resList = append(resList, UpgradeHeroResources{
		Type:  tool.GOLD,
		Count: goldNow,
	})
	resList = append(resList, UpgradeHeroResources{
		Type:  tool.SOLDIER,
		Count: soldierNow,
	})

	return resList

}
