package server

import (
	"fmt"

	"awesomeProject/domain/obj"

	"github.com/jinzhu/gorm"
)

/**

@param resourcesType 资源类型

@return map[int]string  资源类型 => 属性
*/
func ReturnResources(resourcesType int) map[int]string {

	resourcesMap := make(map[int]string)

	switch resourcesType {

	case 1:
		resourcesMap[1] = "diligence"

	case 2:
		resourcesMap[2] = "loyalty"

	case 3:
		resourcesMap[3] = "wisdom"

	default:
		resourcesMap[4] = "无此资源类型"
	}

	return resourcesMap

}

/**
计算 用户英雄战斗力  用户总战力  关卡总战力

@param hero              用户英雄结构体
@param userSoldier       用户士兵资源数量
@param sectionFight      关卡战斗力
@param sectionSoldier    关卡士兵资源数量


@return   userFight      用户英雄战斗力
@return   userTotal      用户总战力
@return   SectionTotal   关卡总战力

*/
func heroDamage(hero *obj.TUserHero, userSoldier, sectionFight, sectionSoldier int) (userFight, userTotal, SectionTotal int) {

	//英雄四项属性的和 等于 英雄的战斗力
	userFight = hero.Brave + hero.Diligence + hero.Loyalty + hero.Wisdom

	userTotal = userFight*5 + userSoldier //用户出战总战力

	SectionTotal = sectionFight*5 + sectionSoldier //关卡总战力

	return userFight, userTotal, SectionTotal
}

/**

将数据集转化为接口数据格式

@param resourcesList 用户资源结构体数据集

@return resList      接口格式数据集

*/

func getNewResourcesList(resourcesList []obj.TUserResources) (resList []Resources) {

	//资源集合 重新 组合
	for _, v := range resourcesList {
		resList = append(resList, Resources{
			Type:  v.Type,
			Count: v.ResourcesTotal,
		})
	}

	return resList
}

/**

将数据集转化为接口数据格式

@param resourcesList 用户资源结构体数据集

@return resList      接口格式数据集

*/

func getNewHeroList(userHeroList []obj.TUserHero) (heroList []Hero) {

	//资源集合 重新 组合
	for _, v := range userHeroList {

		heroList = append(heroList, Hero{
			No: v.HeroNo,
			Lv: v.Level,
		})

	}

	return heroList
}

/**

关闭数据库

@param connect 数据库连接类
*/
func CloseDataFailure(connect *gorm.DB) {

	err := connect.Close()
	if err != nil {
		panic("数据库关闭失败")
	}

	defer func() {
		if err != nil {
			fmt.Println(recover())
		}
	}()

}

/**

  事务回滚

*/
func TransactionRollBack(transaction *gorm.DB, err error) {

	if err != nil {
		transaction.Rollback()
	}

	return
}
