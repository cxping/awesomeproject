package server

import (
	"awesomeProject/tool"

	"github.com/stretchr/testify/assert"
	"testing"
)

func TestRegisterFight(t *testing.T) {

	//初始化
	ast := assert.New(t)

	//正确用户账号 ， 正确出战英雄编号， 有效士兵数
	connect, _ := tool.Connection()
	t.Run("正确用户账号,正确出战英雄编号,有效士兵", func(t *testing.T) {

		userAccount := "13275002095"
		fightHeroNo := 1
		Soldier := 1000
		Data, transaction, _ := RegisterFight(connect, userAccount, fightHeroNo, Soldier)
		if Data.Code != 0 { //事务回滚
			transaction.Rollback()
		}

		t.Log("用户账号: 13275002095 ， 英雄编号: 1 , 登记士兵数：1000")
		ast.Equal("success", Data.Info)
	})

	//正确用户账号 ， 正确出战英雄编号， 有效士兵数
	//不能重复登记
	t.Run("正确用户账号，正确出战英雄编号，有效士兵数", func(t *testing.T) {
		connect, _ = tool.Connection()
		userAccount := "A123456"
		fightHeroNo := 1
		Soldier := 1000
		Data, transaction, _ := RegisterFight(connect, userAccount, fightHeroNo, Soldier)
		if Data.Code != 0 { //事务回滚
			transaction.Rollback()
		}

		t.Log("第一次登记 ：用户账号: A123456 ， 英雄编号: 1 , 登记士兵数：1000")
		connect, _ = tool.Connection()
		userAccount = "A123456"
		fightHeroNo = 1
		Soldier = 1000
		Data, transaction, _ = RegisterFight(connect, userAccount, fightHeroNo, Soldier)
		if Data.Code != 0 { //事务回滚
			transaction.Rollback()
		}
		t.Log("第二次登记 ：用户账号: A123456 ， 英雄编号: 1 , 登记士兵数：1000")
		ast.Equal("不能重复登记", Data.Info)
	})

	//错误用户账号 ， 不正确/正确出战英雄编号， 无效/有效士兵数

	t.Run("错误账号,不正确/正确出战英雄编号,无效/有效士兵数", func(t *testing.T) {
		connect, _ = tool.Connection()
		userAccount := "912446605"
		fightHeroNo := 1
		Soldier := 1000
		Data, transaction, _ := RegisterFight(connect, userAccount, fightHeroNo, Soldier)
		if Data.Code != 0 { //事务回滚
			transaction.Rollback()
		}

		t.Log("用户账号: 912446605 ， 英雄编号: 1 , 登记士兵数：1000")
		ast.Equal("数据库查询不到用户UUID", Data.Info)
	})

	//正确用户账号 ， 不正确出战英雄编号， 无效/有效士兵数

	t.Run("正确账号,不正确出战英雄编号,无效/有效士兵数", func(t *testing.T) {
		connect, _ = tool.Connection()
		userAccount := "912446605@qq.com"
		fightHeroNo := 9
		Soldier := 1000
		Data, transaction, _ := RegisterFight(connect, userAccount, fightHeroNo, Soldier)
		if Data.Code != 0 { //事务回滚
			transaction.Rollback()
		}

		t.Log("用户账号: 912446605@qq.com ， 英雄编号: 9 , 登记士兵数：1000")
		ast.Equal("数据库查询不到用户出战的英雄", Data.Info)
	})

	//正确用户账号 ， 正确出战英雄编号， 无效士兵数

	t.Run("正确用户账号，正确出战英雄编号，无效士兵数", func(t *testing.T) {
		connect, _ = tool.Connection()
		userAccount := "912446605@qq.com"
		fightHeroNo := 1
		Soldier := 100000
		Data, transaction, _ := RegisterFight(connect, userAccount, fightHeroNo, Soldier)
		if Data.Code != 0 { //事务回滚
			transaction.Rollback()
		}

		t.Log("用户账号: 912446605@qq.com ， 英雄编号: 1 , 登记士兵数：100000")
		ast.Equal("出战士兵总数大于数据库士兵资源总数", Data.Info)
	})

}
