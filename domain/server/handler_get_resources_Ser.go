package server

import (
	"time"

	"awesomeProject/domain/obj"
	"awesomeProject/tool"

	"github.com/jinzhu/gorm"
)

func GetResources(connect *gorm.DB, userAccount string, resourcesType int) *CommonResp {

	var (
		heroList      obj.TUserHero      //用户英雄结构体
		userResources obj.TUserResources //用户资源结构体
	)
	defer CloseDataFailure(connect)

	// map[int]string  资源类型 => 属性
	resourcesMap := ReturnResources(resourcesType)
	//判断资源类型符合条件
	if resourcesMap[4] == "无此资源类型" {
		return &CommonResp{
			Code: 9,
			Info: "资源类型异常",
			Data: struct{}{}}
	}

	//获取英雄列表
	list, userId, err := heroList.GetAllHeroByUserAccount(connect, userAccount, resourcesMap[resourcesType])

	if err != nil { //获取列表失败时
		return &CommonResp{
			Code: 4,
			Info: "获取用户英雄列表失败",
			Data: struct{}{}}
	}

	//查看资源收取时间有没满1分钟 且 获取资源最新数量
	resourcesInfo, err := userResources.GetLastTime(connect, resourcesType, userId)

	if err != nil { //获取资源最后收取时间失败
		return &CommonResp{
			Code: 5,
			Info: "获取资源最后收取时间失败",
			Data: struct{}{}}
	}

	//获取当前时间
	now := time.Now()
	//当前时间与最后一次资源收取时间 进行比较
	subM := now.Sub(resourcesInfo.LastResourceTime)

	if int(subM.Minutes()) < 1 { //当前时间与最后一次收取资源时间差在1分钟以内
		return &CommonResp{
			Code: 6,
			Info: "收取资源时间未满1分钟",
			Data: struct{}{}}
	}

	//计算用户相关属性和
	attributeSum := AttributeSum(list, resourcesType)

	//属性差值 = 属性总数  -  初始化属性值
	AttributeDiff := attributeSum - tool.INIT_ATTRIBUTE

	//收取资源后
	resourcesTotal := (AttributeDiff/3)*10 + resourcesInfo.ResourcesTotal + 100

	//更新相关资源总数
	err = userResources.IncreaseResources(connect, resourcesType, resourcesTotal, userId)
	if err != nil { //收取资源更新失败时
		return &CommonResp{
			Code: 7,
			Info: "收取资源更新失败",
			Data: struct{}{}}
	}

	//获取玩家当前资源
	resourcesList, err := userResources.GetResources(connect, userId)
	if err != nil { //收取资源成功后，获取玩家资源失败
		return &CommonResp{
			Code: 8,
			Info: "收取资源成功后，获取玩家资源失败",
			Data: struct{}{}}
	}

	//资源集合 重新 组合
	resList := getNewResourcesList(resourcesList)

	return &CommonResp{
		Code: 0,
		Info: "success",
		Data: ResourcesList{Resource: resList},
	}

}

/**

根据组员类型计算用户相关属性和

@param   heroList         用户英雄集合
@param   resourcesType    资源类型

@return  attributeSum     属性和
*/
func AttributeSum(heroList []*obj.TUserHero, resourcesType int) (attributeSum int) {

	//合计用户资源相关属性值
	for _, hero := range heroList {

		switch resourcesType {
		case int(tool.FOOD):
			attributeSum = attributeSum + hero.Diligence //英雄勤勉
		case int(tool.SOLDIER):
			attributeSum = attributeSum + hero.Loyalty //英雄忠诚
		case int(tool.GOLD):
			attributeSum = attributeSum + hero.Wisdom //英雄智慧
		}
	}

	return attributeSum

}
