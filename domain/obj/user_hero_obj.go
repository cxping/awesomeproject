package obj

import (
	"awesomeProject/tool"

	"github.com/jinzhu/gorm"
)

type TUserHero struct {
	Id        string //Id
	UserId    string //用户表的UUID
	HeroNo    int    //初始化英雄表的UUID
	Level     int    //英雄等级
	Wisdom    int    //英雄智慧
	Loyalty   int    //英雄忠诚
	Brave     int    //英雄英勇
	Diligence int    //英雄勤勉
}

//设置与数据库对应的表名
func (TUserHero) TableName() string {
	return "t_user_hero"
}

/**

创建用户初始化英雄

@param  transaction 事务
@param  userUId     用户表UUID

@return err 错误

*/
func (userHero *TUserHero) CreateInitUserHero(transaction *gorm.DB, userUId string) error {

	var initHero Hero
	//先获取系统初始化英雄  id = 1
	initHero.GetInitUserHero(transaction)

	//生成UUID
	uuid := tool.CreateUUID()

	createUserHero := TUserHero{
		Id:        uuid,
		UserId:    userUId,
		HeroNo:    initHero.No,
		Level:     tool.HERO_LEVEL,
		Wisdom:    initHero.InitWisdom,
		Loyalty:   initHero.InitLoyalty,
		Brave:     initHero.InitBrave,
		Diligence: initHero.InitDiligence,
	}

	//创建用户初始化英雄
	err := transaction.Create(&createUserHero).Error

	return err

}

/**
获取用户所有英雄

@param connect  数据库连接类
@param userUId  用户UUID

*/
func (userHero *TUserHero) GetAllHero(connect *gorm.DB, userUId string) []TUserHero {

	var heroList []TUserHero //数据库映射的用户英雄结构体

	connect.Select("hero_no,level").Where("user_id = ?", userUId).Find(&heroList)

	return heroList
}

/**
通过用户账号和英雄编号获取用户英雄信息

@param connect  数据库连接类
@param userUId  用户UUID
@param heroNo   英雄编号

@return  返回查找到的用户英雄结构体

*/
func (userHero *TUserHero) GetHeroInfo(connect *gorm.DB, userId string, heroNo int) (*TUserHero, error) {

	err := connect.Select("id,user_id,level,wisdom,loyalty,brave,diligence").
		Where("user_id = ? And hero_no = ?", userId, heroNo).Find(userHero).Error

	return userHero, err
}

/**
英雄升级，更新英雄相关属性

@param connect        数据库连接类
@param userHeroId     用户英雄表Id
@param heroLevel      英雄等级
@param heroBrave      英雄英勇值
@param heroDiligence  英雄勤勉值
@param heroLoyalty    英雄忠诚值
@param heroWisdom     英雄智慧值

@return  heroLevelNow 英雄最新等级
@return  err
*/

func (userHero *TUserHero) UpGradeHeroById(transaction *gorm.DB, userHeroInfo *TUserHero, userHeroId string, heroLevel int, heroBrave int, heroDiligence int,
	heroLoyalty int, heroWisdom int) (heroLevelNow int, err error) {

	err = transaction.Model(&userHeroInfo).Updates(map[string]interface{}{
		"Level":     heroLevel,     // 更新英雄等级+1
		"Brave":     heroBrave,     // 更新英雄英勇值+3
		"Diligence": heroDiligence, // 更新英雄勤勉值+1
		"Loyalty":   heroLoyalty,   // 更新英雄忠诚值+1
		"Wisdom":    heroWisdom,    // 更新英雄智慧值+1
	}).Error

	return userHeroInfo.Level, err
}

/**

通过用户账号（user_account）来获取用户所有英雄列表

@param connect        数据库连接类
@param userAccount    用户账号 （user_account）
@param attribute      资源类型对应的属性名字


@return  heroList     用户所有英雄列表
@return  userID       用户UUID
@return  err          错误
*/
func (userHero *TUserHero) GetAllHeroByUserAccount(connect *gorm.DB, userAccount string, attribute string) (heroList []*TUserHero, userId string, err error) {

	var User TUser

	//获取到用户UUID
	user, err := User.GetUUId(connect, userAccount)
	if err != nil {
		return nil, "", err
	}

	//获取用户所有英雄对应的属性值

	err = connect.Select(attribute).Where("user_id = ?", user.Id).Find(&heroList).Error
	if err != nil {
		return nil, "", err
	}

	return heroList, user.Id, nil

}

/**

用户获取新英雄

@param connect        数据库连接类
@param heroNo         英雄编号
@param userId         用户UUID

@return err           错误

*/
func (userHero *TUserHero) GetNewHero(connect *gorm.DB, heroNo int, userId string) error {

	var hero Hero

	//查找新英雄信息
	heroInfo, err := hero.GetHero(connect, heroNo)
	if err != nil {
		return err
	}

	newHero := TUserHero{
		Id:        tool.CreateUUID(),
		UserId:    userId,
		HeroNo:    heroNo,
		Level:     tool.HERO_LEVEL,
		Wisdom:    heroInfo.InitWisdom,
		Loyalty:   heroInfo.InitLoyalty,
		Brave:     heroInfo.InitBrave,
		Diligence: heroInfo.InitDiligence,
	}

	err = connect.Create(&newHero).Error
	if err != nil {
		return err
	}
	return nil

}

/**

查询用户是否有该英雄

@param connect        数据库连接类
@param heroNo         英雄编号
@param userId         用户UUID

@return err           错误

*/
func (userHero *TUserHero) CheckHero(connect *gorm.DB, heroNo int, userId string) (*TUserHero, error) {

	err := connect.Where("user_id = ? AND hero_no = ?", userId, heroNo).Find(&userHero).Error

	return userHero, err
}
