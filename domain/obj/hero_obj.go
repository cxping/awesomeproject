package obj

import (
	"awesomeProject/tool"

	"github.com/jinzhu/gorm"
)

type Hero struct {
	No            int    //英雄编号（前端自己输 ， 后端不进行生成）
	Checkpoint    int    //英雄关卡
	Name          string //英雄名字
	InitLevel     int    //初始化等级
	InitWisdom    int    //初始化智慧
	InitLoyalty   int    //初始化忠诚
	InitBrave     int    //初始化英勇
	InitDiligence int    //初始化勤勉
}

// 设置Hero的表名为`hero`
func (Hero) TableName() string {
	return "hero"
}

/**
      获取用户初始化的英雄
        id = 1 的英雄

@parameter connect 数据库DB类

@return    *Hero   初始化英雄结构体
*/
func (hero *Hero) GetInitUserHero(connect *gorm.DB) *Hero {

	//根据主键来获取记录
	connect.Where("no = ?", tool.INIT_USER_HERO_NUMBER).First(&hero)

	return hero
}

/**

通过英雄编号获取英雄信息

@parameter connect 数据库DB类
@parameter heroNo  英雄编号

@return    *Hero   初始化英雄结构体
@return    error   错误
*/
func (hero *Hero) GetHero(connect *gorm.DB, heroNo int) (*Hero, error) {

	//根据主键来获取记录
	err := connect.Where("no = ?", heroNo).First(&hero).Error

	return hero, err
}
