package routers

import (
	"awesomeProject/handler"
	"net/http"
)

func RegisterRouters() {

	//注册
	http.HandleFunc("/register", handler.HandlerBase(handler.Register))
	//登录
	http.HandleFunc("/login", handler.HandlerBase(handler.Login))
	//资源获取
	http.HandleFunc("/get_resource", handler.HandlerBase(handler.GetResource))
	//登记大厅匹配
	http.HandleFunc("/match_fight", handler.HandlerBase(handler.MatchFight))
	//登记游戏大厅
	http.HandleFunc("/register_fight", handler.HandlerBase(handler.RegisterFight))
	//开始关卡战斗
	http.HandleFunc("/section_fight", handler.HandlerBase(handler.SectionFight))
	//升级英雄
	http.HandleFunc("/upgrade_hero", handler.HandlerBase(handler.UpgradeHero))

}
