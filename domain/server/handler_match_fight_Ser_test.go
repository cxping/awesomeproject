package server

import (
	"awesomeProject/domain/obj"
	"awesomeProject/tool"

	"github.com/stretchr/testify/assert"
	"testing"
)

func TestMatchFight(t *testing.T) {

	//初始化
	ast := assert.New(t)

	//错误用户账号 , 正确/错误出战英雄编号
	db, _ := tool.Connection()
	t.Run("错误用户账号,正确/错误出战英雄编号", func(t *testing.T) {
		MatchHallList["1"] = &MatchHall{HeroNo: 1, HeroFight: 13, Soldier: 500, IsGame: false}
		MatchHallList["2"] = &MatchHall{HeroNo: 1, HeroFight: 13, Soldier: 500, IsGame: false}

		userAccount := "912446605"
		HeroNo := 1
		Data, transaction := MatchFight(db, userAccount, HeroNo)
		//事务回滚
		if Data.Code != 0 {
			transaction.Rollback()
		}
		t.Log("用户账号为：912446605 , 出战英雄编号：1")
		ast.Equal("查询不到攻击方的UUID", Data.Info)
	})

	//正确用户账号 ， 正确/错误出战英雄编号

	t.Run("正确用户账号 ，错误出战英雄编号", func(t *testing.T) {
		db, _ = tool.Connection()
		MatchHallList["1"] = &MatchHall{HeroNo: 1, HeroFight: 13, Soldier: 500, IsGame: false}
		MatchHallList["2"] = &MatchHall{HeroNo: 1, HeroFight: 13, Soldier: 500, IsGame: false}

		userAccount := "13275002095"
		HeroNo := 5
		Data, transaction := MatchFight(db, userAccount, HeroNo)
		//事务回滚
		if Data.Code != 0 {
			transaction.Rollback()
		}
		t.Log("用户账号为：13275002095 , 出战英雄编号：1")
		ast.Equal("查询英雄信息出现错误", Data.Info)
	})

	//正确用户账号 ， 正确出战英雄编号
	//由于某种原因，导致用户数据库无士兵资源
	t.Run("正确用户账号 ，正确出战英雄编号，数据库无士兵资源", func(t *testing.T) {
		db, _ = tool.Connection()
		MatchHallList["1"] = &MatchHall{HeroNo: 1, HeroFight: 13, Soldier: 500, IsGame: false}
		MatchHallList["2"] = &MatchHall{HeroNo: 1, HeroFight: 13, Soldier: 500, IsGame: false}

		userAccount := "C123456"
		HeroNo := 1
		Data, transaction := MatchFight(db, userAccount, HeroNo)
		//事务回滚
		if Data.Code != 0 {
			transaction.Rollback()
		}

		t.Log("用户账号为：C123456 , 出战英雄编号：1")
		ast.Equal("查询用户士兵资源出现错误", Data.Info)
	})

	//正确用户账号 ， 正确出战英雄编号
	//攻击方胜利
	t.Run("正确用户账号 ，正确出战英雄编号，攻击方胜利", func(t *testing.T) {
		db, _ = tool.Connection()
		MatchHallList["1"] = &MatchHall{HeroNo: 1, HeroFight: 13, Soldier: 500, IsGame: false}
		MatchHallList["2"] = &MatchHall{HeroNo: 1, HeroFight: 13, Soldier: 500, IsGame: false}

		userAccount := "13275002095"
		HeroNo := 1
		Data, transaction := MatchFight(db, userAccount, HeroNo)
		//事务回滚
		if Data.Code != 0 {
			transaction.Rollback()
		}
		t.Log("用户账号为：13275002095 , 出战英雄编号：1")
		ast.Equal("success", Data.Info)
	})

	//正确用户账号 ， 正确出战英雄编号
	//数据库无防守方信息
	t.Run("正确用户账号 ，正确出战英雄编号，数据库无防守方信息", func(t *testing.T) {
		db, _ = tool.Connection()
		MatchHallList["111"] = &MatchHall{HeroNo: 1, HeroFight: 13, Soldier: 1000, IsGame: false}
		MatchHallList["111"] = &MatchHall{HeroNo: 1, HeroFight: 13, Soldier: 1000, IsGame: false}

		userAccount := "L123456"
		HeroNo := 1
		Data, transaction := MatchFight(db, userAccount, HeroNo)
		//事务回滚
		if Data.Code != 0 {
			transaction.Rollback()
		}
		t.Log("用户账号为：L123456 , 出战英雄编号：1")
		ast.Equal("查询防守方用户信息出现错误", Data.Info)
	})

	//正确用户账号 ， 正确出战英雄编号
	//数据库无防守方士兵资源
	t.Run("正确用户账号 ，正确出战英雄编号，数据库无防守方资源", func(t *testing.T) {
		db, _ = tool.Connection()
		MatchHallList["4"] = &MatchHall{HeroNo: 1, HeroFight: 13, Soldier: 10000, IsGame: false}
		MatchHallList["4"] = &MatchHall{HeroNo: 1, HeroFight: 13, Soldier: 10000, IsGame: false}

		userAccount := "L123456"
		HeroNo := 1
		Data, transaction := MatchFight(db, userAccount, HeroNo)
		//事务回滚
		if Data.Code != 0 {
			transaction.Rollback()
		}
		t.Log("用户账号为：L123456 , 出战英雄编号：1")
		ast.Equal("查询防守方士兵资源出现错误", Data.Info)
	})

	//正确用户账号 ， 正确出战英雄编号
	//判断防守方剩余士兵资源数量小于0并执行对应操作
	t.Run("正确用户账号 ，正确出战英雄编号，防守方剩余士兵资源数量小于0", func(t *testing.T) {
		db, _ = tool.Connection()
		var matchHall obj.TMatchHall
		MatchHallList["1"] = &MatchHall{HeroNo: 1, HeroFight: 13, Soldier: 5000, IsGame: false}

		defenseUuid := "1"
		reduceDefenseSoldier := 10000
		Data, _ := Judge(db, matchHall, defenseUuid, reduceDefenseSoldier)

		t.Log("用户账号为：L123456 , 出战英雄编号：1")
		ast.Nil(Data)
	})

	//正确用户账号 ， 正确出战英雄编号
	//判断防守方剩余士兵资源数量大于0并执行对应操作
	t.Run("正确用户账号 ，正确出战英雄编号，防守方剩余士兵资源数量大于0", func(t *testing.T) {
		db, _ = tool.Connection()
		var matchHall obj.TMatchHall
		MatchHallList["1"] = &MatchHall{HeroNo: 1, HeroFight: 13, Soldier: 5000, IsGame: false}

		defenseUuid := "1"
		reduceDefenseSoldier := 100
		Data, _ := Judge(db, matchHall, defenseUuid, reduceDefenseSoldier)

		t.Log("用户账号为：L123456 , 出战英雄编号：1")
		ast.Nil(Data)
	})

}
