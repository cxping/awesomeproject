package handler

import (
	"net/http"

	"awesomeProject/domain/server"
	"awesomeProject/tool"
)

// 请求参数  结构体
type registerPara struct {
	UserId   string `valid:"required"` //用户Id(用户Id,全服唯一)
	Name     string `valid:"required"` //用户名字(用户名，全服唯一)
	Password string `valid:"required"` //用户密码
}

//返回参数  结构体
type registerResp struct {
	Code int    `json:"code"` //返回码
	Info string `json:"Info"` //错误信息
}

//用户注册
func Register(w http.ResponseWriter, r *http.Request) {

	var (
		//声明注册请求结构体
		registerRequest registerPara
		//定义该接口请求方式
		requestType = "POST"
	)

	//请求方式验证、数据格式验证、反序列化
	ErrResp, connect := Check(r, requestType, &registerRequest)
	if ErrResp != nil {
		conErr := registerResp{
			Code: ErrResp.Code,
			Info: ErrResp.Info,
		}
		err := tool.WriteResponse(w, conErr)
		throwPanic(err, "请求响应出现数据化序列化或响应数据写入出现错误")
		return
	}

	//获取用户账号（全服唯一）
	userAccount := registerRequest.UserId
	//获取用户昵称
	name := registerRequest.Name
	//获取用户密码
	password := registerRequest.Password

	//用户注册
	commonResp, transaction := server.RegisterLogic(connect, userAccount, name, password)
	if commonResp.Code != 0 { //事务回滚
		transaction.Rollback()
	}

	Resp := UpgradeHeroResp{
		Code: commonResp.Code,
		Info: commonResp.Info,
		Data: commonResp.Data,
	}
	err := tool.WriteResponse(w, Resp)
	throwPanic(err, "请求响应出现数据化序列化或响应数据写入出现错误")
	return
}
