package models

/**

  用户关卡表（用户当前解锁的关卡 ）

  用户 与  关卡 关系   1  对  1
   只记录新解锁的关卡记录

*/
type TUserFight struct {
	Id           string //Id
	UserId       string //用户表的UUID
	FightSoldier int    //关卡士兵数
	FightNo      int    //关卡编号
}
