package handler

import (
	"net/http"

	"awesomeProject/domain/server"
	"awesomeProject/tool"
)

//请求参数
type ResourcePara struct {
	UserId       string `valid:"required"` //用户Id => UserAccount用户账号
	ResourceType int    `valid:"required"` //资源类型
}

//相应参数
type ResourcesResp struct {
	Code int         `json:"Code"`
	Info string      `json:"Info"`
	Data interface{} `json:"Data"`
}

//收取资源
func GetResource(w http.ResponseWriter, r *http.Request) {

	var (
		reqPara   ResourcePara
		reqMethod = "PUT"
	)

	//请求方式验证、数据格式验证、反序列化
	ErrResp, connect := Check(r, reqMethod, &reqPara)
	if ErrResp != nil {

		Resp := ResourcesResp{
			Code: ErrResp.Code,
			Info: ErrResp.Info,
			Data: ErrResp.Data,
		}
		err := tool.WriteResponse(w, Resp)
		throwPanic(err, "请求响应出现数据化序列化或响应数据写入出现错误")
		return
	}

	//根据用户账号的资源类型 查询数据库中对应的资源 是否能否收取
	userAccount := reqPara.UserId
	resourcesType := reqPara.ResourceType

	CommonResp := server.GetResources(connect, userAccount, resourcesType)

	Resp := ResourcesResp{
		Code: CommonResp.Code,
		Info: CommonResp.Info,
		Data: CommonResp.Data,
	}
	err := tool.WriteResponse(w, Resp)
	throwPanic(err, "请求响应出现数据化序列化或响应数据写入出现错误")
	return

}
