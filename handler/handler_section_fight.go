package handler

import (
	"net/http"

	"awesomeProject/domain/server"
	"awesomeProject/tool"
)

//请求参数结构体
type SectionFightPara struct {
	//用户Id  => 用户账号
	UserId string `valid:"required"`
	//出战英雄编号
	HeroNo int `valid:"required"`
}

//响应结构体
type SectionFightResp struct {
	//状态码
	Code int `json:"Code"`
	//请求是否成功
	Info string `json:"Info"`
	//数据
	Data interface{} `json:"Data"`
}

//开始关卡战斗
func SectionFight(w http.ResponseWriter, r *http.Request) {

	var (

		//声明请求结构体
		sectionFightRequest SectionFightPara
		//请求方法
		reqMethod = "PUT"
	)

	//请求方式验证、数据格式验证、反序列化
	ErrResp, connect := Check(r, reqMethod, &sectionFightRequest)
	if ErrResp != nil {

		errResp := SectionFightResp{
			Code: ErrResp.Code,
			Info: ErrResp.Info,
			Data: struct{}{},
		}
		err := tool.WriteResponse(w, errResp)
		throwPanic(err, "请求响应出现数据化序列化或响应数据写入出现错误")
		return

	}

	//初始化用户Id  => 用户账号（数据库）
	userAccount := sectionFightRequest.UserId
	//用户出战英雄编号
	heroNo := sectionFightRequest.HeroNo

	//英雄开始战斗逻辑
	commonResp, transaction := server.SectionFight(connect, userAccount, heroNo)
	//事务回滚
	if commonResp.Code != 0 {
		transaction.Rollback()
	}

	Resp := server.CommonResp{
		Code: commonResp.Code,
		Info: commonResp.Info,
		Data: commonResp.Data,
	}
	err := tool.WriteResponse(w, Resp)
	throwPanic(err, "请求响应出现数据化序列化或响应数据写入出现错误")
	return

}
