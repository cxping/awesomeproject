package models

import "time"

/**

用户匹配大厅表

    用户 -> 大厅等级表
    关系    1   对  1

*/

type TMatchHall struct {
	Id          string    //UUId
	UserId      string    //用户UUID
	PlayHeroNo  int       //出战英雄编号
	PlaySoldier int       //出战士兵资源数
	HeroFight   int       //出战英雄战斗力
	DeleteAt    time.Time //是否删除
}
